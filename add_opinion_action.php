<?php
include_once("head.php");
?>

<div id="navigation">

    <div id="tabs">

        <ul>
            <!--<li><a href="index.php"><span>Providers</span></a></li>-->
            <li><a href="strategy.php"><span>Strategies</span></a></li>
            <li ><a href="news.php"><span>New views</span></a></li>
            <li><a href="expert_list.php"><span>Experts</span></a></li>
             <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
        </ul>

        <div class="clearer">&nbsp;</div>

    </div>

</div>

<div class="spacer h5"></div>

<div id="main">

    <div class="left" id="main_left">

        <div id="main_left_content">

            <div class="post">

                <div class="post_title">
                    <h1>Result</h1>
                </div>

                <div class="post_body">

                    <?php
                    if (isSet($_SESSION['id_expert'])) {
                        if (isSet($_GET['int_id'])) {

                            $jumpMenu_A = $_POST['jumpMenu'];
                            $comments = addslashes($_POST['comments']);
                            $id_expert = $_SESSION['id_expert'];
                            $id_interrogation = $_GET['int_id'];

//                                $alt = new Alternative();
//                                $id_alt = $alt->getIdA($id_interrogation);
                            $op = new Opinion();
                            if ($_GET['type'] == "strategy") {
                                $op->insertOpinion($comments, $jumpMenu_A, $id_expert, $id_interrogation, 'strategy');
								$op->insertMarks($id_expert,$id_interrogation);
                            } else {
                                $op->insertOpinion($comments, $jumpMenu_A, $id_expert, $id_interrogation);
                            }
                            $exp = new Experience();
                            $exp->updateExperience($id_expert, $id_interrogation);
                            print("<h2>You have left a comment</h2>");
                            print("<BR><a href = \"interrogation.php?i_id=$id_interrogation\"> Back<BR></a>");
							

							
                        } else {
                            print("<h2>Error!</h2>");
                        }
                    } else {
                        print("<h2>You haven't loged in!</h2>");
                    }
                    ?>

                </div>

            </div>

        </div>

    </div>

<?php
                    include_once("end.php");
?>