<div class="right" id="main_right">
	
			<div id="sidebar">
				<div class="box">
					<div class="box_title">Authentification</div>
					<div class="box_body">
						<ul>
						<?php 
						if(isSet($_SESSION['id_expert']))
						{
							print("<li><a href=\"out.php\">Log out</a> </li>");
							$page=new HomePage();
							$page->printLink($_SESSION['id_expert']);
						}
						else
						{
						print("<li><a href=\"enter.php\">Log in</a> </li>
							<li><a href=\"registration.php\">Registration</a> </li>");
						}
						?>
							
						</ul>
					</div>
					<div class="box_bottom"></div>
				</div>

<div class="box">
					<div class="box_title">Statistics</div>
					<div class="box_body">
						<ul>
							<li><a href="statistics_interrogation.php?type=strategy">Strategies statistics</a> </li>
                                                        <!-- <li><a href="statistics_interrogation.php?type=interrogation">Providers Statistics</a> </li> -->
                            
							<li><a href="statistics_expert.php?page=1">Experts statistics</a> </li>
							
						</ul>
					</div>
					<div class="box_bottom"></div>
				</div>
				<div class="box">
					<div class="box_title">Rate factors</div>
					<div class="box_body">
						<ul>
                            <li><a href="compare_fact.php">Compare factories</a></li>
						</ul>
					</div>
					<div class="box_bottom"></div>
				</div>
                <?php if($_SESSION['is_admin']) { ?>
                <div class="box">
					<div class="box_title">Calculation</div>
					<div class="box_body">
						<ul>
							<!-- <li><a href="new_interrogation.php">Add a provider</a></li> -->
							<li><a href="calc_cc.php">Coefficient of concordance</a></li>
                            <li><a href="calc_cf.php">Calculate vector</a></li>
                            <li><a href="calc_range_mark_pr.php">Calculate range mark of provider</a></li>
						</ul>
					</div>
					<div class="box_bottom"></div>
				</div>
				<div class="box">
					<div class="box_title">Creation</div>
					<div class="box_body">
						<ul>
							<!-- <li><a href="new_interrogation.php">Add a provider</a></li> -->
							<li><a href="calculate.php">Make calculations</a></li>
                            <li><a href="main_graph.php">Build graph of strategy</a></li>
                            <li><a href="factors.php">Factors</a></li>
                            <li><a href="create_provider.php">Create provider</a></li>
						</ul>
					</div>
					<div class="box_bottom"></div>
				</div>
                 <?php } ?>
                
				
				

			</div>
		</div>

		<div class="clearer">&nbsp;</div>
<?php 
$string=<<< BEG
	</div>

	<div id="footer">

		<div class="left">Kate Shevchenko 2013 </div>

		

		<div class="clearer">&nbsp;</div>
	
	</div>

</div>
</body>
</html>
BEG;
print($string);
?>