<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li ><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                 <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Adding a mark tp the view</h1>
					</div>
				
					<div class="post_body">
					
						<?php 
						if(isSet($_SESSION['id_expert']))
						{
							if(isSet($_POST['jumpMenu_estimation'])&&isSet($_POST['comments_estimation'])&&isSet($_GET['int_id'])&&isSet($_GET['op_id']))
							{
							$i=new EstimationOpinion();
							$jumpMenu_estimation = $_POST['jumpMenu_estimation'];
							$comments_estimation=addslashes($_POST['comments_estimation']);
							$id_expert=$_SESSION['id_expert'];
							$id_interrogation=$_GET['int_id'];
							$id_opinion=$_GET['op_id'];
							if($i->isOtherExpert($id_opinion,$id_expert))
							{
								print("<h2>You can't comment your marks!</h2>");
								 print("<br><p><a href = \"estimation.php?i_id=$id_interrogation&o_id=$id_opinion\">Back to the expers opinion
								 </a></p>");
								 //print("<p><a href = \"interrogation.php?i_id=$id_interrogation\">Back to the pool</a></p>");
							}
							else{
							$i->insertEstimationOpinion($id_expert,$id_opinion,$jumpMenu_estimation,$comments_estimation);
							$exp=new Experience();
							$exp->updateExperience($id_expert,$id_interrogation);
							print("<h2>You have left a comment</h2>");
							print("<BR><p><a href = \"estimation.php?i_id=$id_interrogation&o_id=$id_opinion\">Back to the experts opinion</a></p>");
								// print("<p><a href = \"interrogation.php?i_id=$id_interrogation\">Back to the pool </a></p>");
							}
							}
							else
							{
								print("<h2>Error!</h2>");	
							}
						
						}
						else
						{
							print("<h2>You haven't logged in !</h2>");
						}
						?>
					
					</div>

				</div>
		
			</div>

		</div>

		<?php 
include_once("end.php");
?>