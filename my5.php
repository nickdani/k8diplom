<?php
//обратится к базе данных и сформировать строку такого формата и присвоить переменной 
$strdata = 'data: {nodes:[
							{ id: "10", label: "Mr1mr0Mc1mc1", provider: "", group: ""},
							{ id: "12", label: "Mr1mr0Mc1mc2", provider: "", group: ""},
							{ id: "23", label: "Mr1mr0Mc2mc3", provider: "", group: ""},
							{ id: "34", label: "Mr1mr0Mc3mc4", provider: "", group: ""},	
							{ id: "35", label: "Mr1mr0Mc3mc5", provider: "", group: ""},	
							{ id: "46", label: "Mr1mr0Mc4mc6", provider: "", group: ""},
							{ id: "11", label: "Mr1mr1Mc1mc1", provider: "", group: ""},
							{ id: "12", label: "Mr1mr1Mc1mc2", provider: "", group: ""},
							{ id: "22", label: "Mr1mr1Mc2mc3", provider: "", group: ""},
							{ id: "34", label: "Mr1mr1Mc3mc4", provider: "", group: ""},
							{ id: "35", label: "Mr1mr1Mc3mc5", provider: "", group: ""},
							{ id: "46", label: "Mr1mr1Mc4mc6", provider: "", group: ""}],
edges: [
{ id: "1023", target: "23", source: "10", provider: "", group: ""},	
{ id: "1123", target: "23", source: "11", provider: "", group: ""},
	{ id: "2234", target: "34", source: "22", provider: "", group: ""},	
	{ id: "2235", target: "35", source: "22", provider: "", group: ""},	
	{ id: "3346", target: "46", source: "34", provider: "", group: ""},
		{ id: "3446", target: "46", source: "34", provider: "", group: ""}	]}';
			?>
<!doctype html>
<html><head>
<title>Cytoscape Web example</title>

<!-- JSON support for IE (needed to use JS API) -->
<script type="text/javascript" src="min/json2.min.js"></script>

<!-- Flash embedding utility (needed to embed Cytoscape Web) -->
<script type="text/javascript" src="min/AC_OETags.min.js"></script>

<!-- Cytoscape Web JS API (needed to reference org.cytoscapeweb.Visualization) -->
<script type="text/javascript" src="min/cytoscapeweb.min.js"></script>

<script type="text/javascript">
    window.onload=function() {
        // id of Cytoscape Web container div
        var div_id = "cytoscapeweb";
        
        // you could also use other formats (e.g. GraphML) or grab the network data via AJAX
       
        var networ_json = {
			dataSchema: {
			nodes: [ { name: "label", type: "string" },
					 { name: "score", type: "number" },
					 { name: "provider", type: "string"},
					 { name: "group", type: "string" }  
			],
			 
			edges: [ { name: "label", type: "string" },
					 { name: "weight", type: "number" },
					 { name: "provider", type: "string" },
					 { name: "group", type: "string" }, 
					 { name: "directed", type: "boolean", defValue: true} 
			]
			},
			<?php echo $strdata; ?>
			
		};
	
		// initialization options
		var options = {
			// where you have the Cytoscape Web SWF
			swfPath: "swf/CytoscapeWeb",
			// where you have the Flash installer SWF
			flashInstallerPath: "swf/playerProductInstal"
		};
      
        // visual style we will use
        var visual_style = {
            global: {backgroundColor: "#ABCFD6"},
			
            nodes: {
                shape: "ROUNDRECT",
                borderWidth: 2,
                borderColor: "#ffffff",
                label: { passthroughMapper: { attrName: "id" } },
    			labelFontSize: 10,
    			labelFontWeight: "bold",
				size: {
					defaultValue: 25,
					continuousMapper: { attrName: "weight", minValue: 25, maxValue: 75 }
				},
				
				color: {
					discreteMapper: {
						attrName: "id",
						entries: [
							{ attrValue: 1, value: "#FF281F" },
							{ attrValue: 2, value: "#FF281F" },
							{ attrValue: 3, value: "#dddd00" },
							{ attrValue: 4, value: "#dddd00" },
							{ attrValue: 5, value: "#dddd00" }
						]
				}
             },
                labelHorizontalAnchor: "center"
            },
            edges: {
                width: 3,
				
				color: {
					discreteMapper: {
						attrName: "provider",
						entries: [
							{ attrValue: "LLP", value: "#F77F00" },
							{ attrValue: "3LP", value: "#6E4993" },
							{ attrValue: "4PL", value: "#ADF77B" },
							{ attrValue: "", value: "#0B94B1" },
							
						]
				}
             },
                label: { passthroughMapper: { attrName: "provider" } 
			},
			
			
			
    		labelFontSize: 10,
    		labelFontWeight: "bold",
               
            }
        };
        var vis = new org.cytoscapeweb.Visualization(div_id, options);
        
        //vis.ready(function() {
            // set the style programmatically
           // document.getElementById("color").onclick = function(){
                //visual_style.global.backgroundColor = rand_color();
                //vis.visualStyle(visual_style);
           // };
        //});
        vis.ready(function() {
			vis.addContextMenuItem("Set provider as 3LP", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "3LP";
			vis.updateData([edge]);
		
		});
		
		vis.addContextMenuItem("Set provider as LLP", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "LLP";
			vis.updateData([edge]);
		});
		vis.addContextMenuItem("Set provider as 4PL", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "4PL";
			vis.updateData([edge]);
		});
	});
	
	//vis.draw.layout ={ name:    "Tree",
    //options: { orientation: "leftToRight", subtreeSpace: 80 }
//};
       
	    
        var draw_options = {
            // your data goes here
            network: networ_json,
            
            // show edge labels too
            edgeLabelsVisible: true,
            
            // let's try another layout
            
           layout: "Tree",
            // set the style at initialisation
            visualStyle: visual_style,
            
            // hide pan zoom
            panZoomControlVisible: false 
        };
        
        vis.draw(draw_options,{network: networ_json});
        
        
    };
</script>
<style>
    * { margin: 0; padding: 0; font-family: Helvetica, Arial, Verdana, sans-serif; }
    html, body { height: 100%; width: 100%; padding: 0; margin: 0; background-color: #f0f0f0; }
    body { line-height: 1.5; color: #000000; font-size: 14px; }
    /* The Cytoscape Web container must have its dimensions set. */
    #cytoscapeweb { width: 100%; height: 80%; }
    #note { width: 100%; text-align: center; padding-top: 1em; }
    .link { text-decoration: underline; color: #0b94b1; cursor: pointer; }
</style>

</head>

<body>
<div id="cytoscapeweb">
    Cytoscape Web will replace the contents of this div with your graph.
</div>
</body>
    
</html>