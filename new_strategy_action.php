<?php
include_once("head.php");
?>


<div id="navigation">

    <div id="tabs">

        <ul>
            <!-- <li><a href="index.php"><span>Providers</span></a></li> -->
            <li><a href="strategy.php"><span>Strategies</span></a></li>
            <li><a href="news.php"><span>New views</span></a></li>
            <li><a href="expert_list.php"><span>Experts</span></a></li>
            <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>

        </ul>

        <div class="clearer">&nbsp;</div>

    </div>

</div>

<div class="spacer h5"></div>

<div id="main">

    <div class="left" id="main_left">

        <div id="main_left_content">

            <div class="post">

                <div class="post_title">
                    <h1>���������</h1>
                </div>

                <div class="post_body">
                    <?php
                    if (isSet($_SESSION['id_expert'])) {
                        if (isSet($_POST['subjects']) && isSet($_POST['comments_interrogation'])) {
                            $id_expert = $_SESSION['id_expert'];

                            $subject = addslashes(trim($_POST['subjects']));
                            $comments_interrogation = addslashes(trim($_POST['comments_interrogation']));
                            $bool = ($subject != "");

                            if ($bool) {
                                $j = new Strategy();
                                $j->newStrategy($subject, $comments_interrogation, $id_expert);

                                print("<h2>You have enterred new strategy</h2>");
                            } else {
                                print("<h2>You have enterred an empty string</h2>");
                            }
                        } else {
                            print("<h2>Error!</h2>");
                        }
                    } else {
                        print("<h2>You haven't logged in!</h2>");
                    }
                    ?>

                </div>

            </div>

        </div>

    </div>

    <?php
                    include_once("end.php");
    ?>