<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
               <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Results</h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
					<div class="post_body">
						<?php
							if(isSet($_POST['new_name'])&&isSet($_POST['new_surname'])&&isSet($_SESSION['id_expert']))
							{
								$home=new HomePage();
								$name=$_POST['new_name'];
								$surname=$_POST['new_surname'];
								$id_expert=$_SESSION['id_expert'];
								$home->updateData($id_expert,$name,$surname);
								print("<h2>You have changed the data</h2>");
							}
							else
							{
								print("Error!");
							}
					   ?>
					  
					  </div>
					 </ol>
					</div>

				</div>
		
			</div>

		</div>

		
<?php 
include_once("end.php");
?>