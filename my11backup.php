<?php
//обратится к базе данных и сформировать строку такого формата и присвоить переменной 
//$filename = "data/first.txt";

$filename = "data/first.txt";
$handle = fopen($filename, "r");
$strdata = fread($handle, filesize($filename));
fclose($handle);
			?>
<!doctype html>
<html><head>
<meta charset="utf-8">
<title>Cytoscape Web example</title>
<link href="css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css">
<link href="css/common.css" rel="stylesheet" type="text/css">
<style type="text/css">
#graph {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	width:500px;
}
.row {
	float:left;
	width:100%;
	clear:both;
	border-bottom:1px solid #98c4e1;
}
.col1, .col2, .col3{
	width:32.5%;
	padding:2px;
	border:none;
	float:left;
}
.scroll {
	height:100px;
	padding:5px;
	width:515px;
	border:1px solid #98c4e1;
	overflow: auto;
}
.head_table {
	background-color:#d3e9fc;
	font-weight:bold;
	text-align:center;
	border-bottom:none !important;
}

</style>
<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/iColorPicker.js"></script>

<!-- JSON support for IE (needed to use JS API) -->
<script type="text/javascript" src="min/json2.min.js"></script>

<!-- Flash embedding utility (needed to embed Cytoscape Web) -->
<script type="text/javascript" src="min/AC_OETags.min.js"></script>

<!-- Cytoscape Web JS API (needed to reference org.cytoscapeweb.Visualization) -->
<script type="text/javascript" src="min/cytoscapeweb.min.js"></script>

<script type="text/javascript">
    var networ_json;
	var vis;
	var propsarr = [];
	var table_gr = [];
	//var mapperarr = [{attrValue: "3PL", value: "#FF0000"},{attrValue: "4PL", value: "#00FF00"},{attrValue: "LLP", value: "#0000FF"}, { attrValue: "", value: "#EEEEEE" }];
	$(function() {
		// hide dialog window
		$("#save_graph_wnd").hide(); 
		$("#alert_d").hide();
		$("#set_par_graph_wnd").hide();
		//Tool bar
		$("#set_defaut_btn").button({icons:{primary:"ui-icon-trash" },text:true});
		$("#save_graph_btn").button({icons:{primary:"ui-icon-disk"},text:true});
		//$("#set_color_btn").button({icons:{primary:"ui-icon-pencil"},text:false});
		$("#set_defaut_btn").click(function(e){
			
			createTableGraph();
			
			/*
			var edges = vis.edges();
			var imax = edges.length;
			for( var i = 0; i<imax; i++){
				 edges[i].data.provider = "";
			}
			$.each(networ_json.data.edges,function(index,val) {
				networ_json.data.edges[index].provider = "";
			});
            vis.updateData(edges);
			*/
		
		});
		$("#save_graph_btn").click(function(e){
			// save data graph
			$("#save_graph_wnd").dialog("option", "title", 'Save graph');
			$("#save_graph_wnd").dialog( "open" );
			// clean message error
			tips_vp.html('&nbsp;');
			$("#save_graph_form #file_name_ti").removeClass("ui-state-error");
			$("#save_graph_form #file_name_ti").val('');
		
		});
		$("#set_color_btn").click(function(e){
			//alert("COOLOLOR");
			iColorShow('color_ti','set_color_btn');
		});
		var file_name = $("#file_name_ti");
		// набор элементов для значений параметра
		var vpFields = $([]).add(file_name);
		// Dialog window
		var tips_vp = $("#tips_vp");
		$("#set_par_graph_wnd").dialog({resizable:true,
			autoOpen:false,
			minHeight: 240,
			minWidth: 350,
			modal: true,
			buttons: {
				"Save": function() {
					//alert('save');
				var sel_obj = vis.selected();
			
				var bypass = { nodes: { }, edges: { } };
				
				var props = {};
				$.each(sel_obj, function(index,value)
				{
					//alert(value.data.id);
					props.color = $("#color_ti").val();
					propsarr[value.data.id] = props;
					
				});
				for (var i=0; i < sel_obj.length; i++) {
					var obj = sel_obj[i];
					 bypass[obj.group][obj.data.id] = propsarr[obj.data.id];
					 $.each(propsarr, function(index,value){
						if(typeof(value) != 'undefined'){
							bypass[obj.group][index] = value;
						}
					});
					 
				}
				vis.visualStyleBypass(bypass);
				var node;
				var edge;
				imax = sel_obj.length;
				for(var i=0; i < imax; i++){
					//alert('XXX = '+sel_obj[i].data.id);
					sel_id = sel_obj[i].data.id;
					$.each(networ_json.data.nodes,function(index,val){
						if(sel_id == val.id){
							val.group = $("#name_ti").val();
							}
						});
					$.each(networ_json.data.edges,function(index,val) {
						if(sel_id == val.id){
							//alert('val='+val.id);
							val.group = $("#name_ti").val();
						}
					});
				}
				$(this).dialog("close");},
				"Cancel": function() {$(this).dialog("close");}
			}
		});
		
		$("#save_graph_wnd").dialog({resizable:true,
		autoOpen: false,
		minHeight: 180,
		minWidth: 350,
		modal: true,
		buttons: {
			"Save": function() {
				var bValid = true;
				vpFields.removeClass("ui-state-error");

				bValid = bValid && checkLength(file_name, 'File name', 3, 20,tips_vp);
				bValid = bValid && checkRegexp(file_name, /^[0-9a-zA-Z]{3,}$/i, 'Only latin letter and number are allowed!',tips_vp);

				if (bValid) {
					// Save graph
					var i=0;
					//var groups = [];
					str_json = 'data: { ';
					$.each(networ_json.data,function(index,val){
						imax = val.length;
							if(i == 0){
								str_json += 'nodes: [ ';
								$.each(networ_json.data.nodes,function(index,val){
									// write nodes
									str_json += '{ id: "'+networ_json.data.nodes[index].id+'", ';
									str_json += 'label: "'+networ_json.data.nodes[index].label+'", ';
									str_json += 'provider: "'+networ_json.data.nodes[index].provider+'", '; 
									str_json += 'group: "'+networ_json.data.nodes[index].group+'" },';
								});
								str_json = str_json.substr(0,str_json.length-1);
								i++;
							} else {
								//  write edges
								str_json += ', edges: [ ';
								$.each(networ_json.data.edges,function(index,val) {
									str_json += '{ id: "'+networ_json.data.edges[index].id+'", ';
									str_json += 'target: "'+networ_json.data.edges[index].target+'", ';
									str_json += 'source: "'+networ_json.data.edges[index].source+'", ';
									str_json += 'provider: "'+networ_json.data.edges[index].provider+'", '; 
									str_json += 'group: "'+networ_json.data.edges[index].group+'" },';
								});
								str_json = str_json.substr(0,str_json.length-1);
							}
							str_json += ' ],';
							if(i == 1){
								// cut comma
								str_json = str_json.substr(0,str_json.length-1);
							}
					});
					str_json += ' }';
					//alert(str_json);
					var post = {};
					post.json = str_json;
					post.name = $("#save_graph_form #file_name_ti").val();
					var url = 'save_graph.php';
					// upload data
					$.post(url,post,function(data,status){
						if(status == 'success'){
							showMessage('Message', '<p>'+data+'</p>', 300, "Close");
						} else {
							// Server error
							showMessage('Message', '<p>File does not save! Server error.</p>'+data, 400, "Close");
						}
					});
					
					
					
					
					$(this).dialog("close");
				}

			},"Cancel": function() {$(this).dialog("close");}
		}
	});
		
        // id of Cytoscape Web container div
        var div_id = "cytoscapeweb";
        
        // you could also use other formats (e.g. GraphML) or grab the network data via AJAX
       
        networ_json = {
			dataSchema: {
			nodes: [ { name: "label", type: "string" },
					 { name: "score", type: "number" },
					 { name: "provider", type: "string"},
					 { name: "group", type: "string",  defValue: "0"}  
			],
			 
			edges: [ { name: "label", type: "string" },
					 { name: "weight", type: "number" },
					 { name: "provider", type: "string" },
					 { name: "group", type: "string",  defValue: "0" }, 
					 { name: "directed", type: "boolean", defValue: true} 
			]
			},
			<?php echo $strdata; ?>
			
		};
	
		// initialization options
		var options = {
			// where you have the Cytoscape Web SWF
			swfPath: "swf/CytoscapeWeb",
			// where you have the Flash installer SWF
			flashInstallerPath: "swf/playerProductInstal"
		};
      
        // visual style we will use
        var visual_style = {
            global: {backgroundColor: "#ABCFD6"},
			
            nodes: {
                shape: "ROUNDRECT",
                borderWidth: 2,
                borderColor: "#ffffff",
                label: { passthroughMapper: { attrName: "provider" } },
    			labelFontSize: 11,
    			labelFontWeight: "bold",
				size: {
					defaultValue: 25,
					continuousMapper: { attrName: "weight", minValue: 25, maxValue: 75 }
				},
				
				/*color: {
					discreteMapper: {
						attrName: "provider",
						entries: []
				}
             },*/
                labelHorizontalAnchor: "center"
            },
            edges: {
                width: 3,
				
				/*color: {
					discreteMapper: {
						attrName: "provider",
						entries: [
							{ attrValue: "LLP", value: "#F77F00" },
							{ attrValue: "3PL", value: "#6E4993" },
							{ attrValue: "4PL", value: "#ADF77B" },
							{ attrValue: "Self-execution", value: "#0B94B1" },
							{ attrValue: "", value: "#999999" }
							
							
						]
				}
             },*/
                label: { passthroughMapper: { attrName: "provider" } 
			},
			
			
			
    		labelFontSize: 11,
    		labelFontWeight: "bold",
               
            }
        };
		//visual_style.nodes.color.discreteMapper.entries = mapperarr;
		//Contextmenu
		
        vis = new org.cytoscapeweb.Visualization(div_id, options);
        
        vis.ready(function() {
			vis.addContextMenuItem("Set provider as 3PL", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			edge.data.provider = "3PL";
			$.each(networ_json.data.edges,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.edges[index].provider = "3PL";
				}
			});
			vis.updateData([edge]);
		
		});
		
		vis.addContextMenuItem("Set provider as LLP", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "LLP";
			$.each(networ_json.data.edges,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.edges[index].provider = "LLP";
				}
			});
			vis.updateData([edge]);
		});
		vis.addContextMenuItem("Set provider as 4PL", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "4PL";
			$.each(networ_json.data.edges,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.edges[index].provider = "4PL";
				}
			});
			vis.updateData([edge]);
		});
		vis.addContextMenuItem("Self-execution", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "Self-execution";
			$.each(networ_json.data.edges,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.edges[index].provider = "Self-execution";
				}
			});
			vis.updateData([edge]);
		});
		
		vis.addContextMenuItem("Add to one group", "none", function(evt) {
    		// Get the right-clicked node:
			$("#set_par_graph_wnd").dialog("option", "title", 'Set group parametras');
			$("#set_par_graph_wnd").dialog( "open" );
		});
		
		vis.addContextMenuItem("Set warehouse provider as 4PL", "nodes",function(evt) {
    		// Get the right-clicked node:
			var node = vis.node(evt.target.data.id);
			node.data.provider = "4PL";
			$.each(networ_json.data.nodes,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.nodes[index].provider = "4PL";
				}
			});
			vis.updateData([node]);
		
		});
		vis.addContextMenuItem("Set warehouse provider as 3PL", "nodes",function(evt) {
    		// Get the right-clicked node:
			var node = vis.node(evt.target.data.id);
			node.data.provider = "3PL";
			$.each(networ_json.data.nodes,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.nodes[index].provider = "3PL";
				}
			});
			vis.updateData([node]);
		
		});
		
		vis.addContextMenuItem("Set provider as 3PL", "edges",function(evt) {
    		// Get the right-clicked node:
			var edge = vis.edge(evt.target.data.id);
			//alert(edge.data.provider = '3LP');
			edge.data.provider = "3PL";
			$.each(networ_json.data.edges,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.edges[index].provider = "3PL";
				}
			});
			vis.updateData([edge]);
		});
		vis.addContextMenuItem("Set warehouse provider as LLP", "nodes",function(evt) {
    		// Get the right-clicked node:
			var node = vis.node(evt.target.data.id);
			node.data.provider = "LLP";
			$.each(networ_json.data.nodes,function(index,val){
				if(val.id == evt.target.data.id){
					networ_json.data.nodes[index].provider = "LLP";
				}
			});
			vis.updateData([node]);
		
		});
	});
	
	var options = { 
		orientation :  "leftToRight" ,
		depthSpace: 55,
		breadthSpace: 20,
		subtreeSpace: 80
	};
      
	    
	var draw_options = {
		// your data goes here
		network: networ_json,
		
		// show edge labels too
		edgeLabelsVisible: true,
		
		// let's try another layout
		
	   layout: {name: "Tree", options: options},
		// set the style at initialisation
		visualStyle: visual_style,
		
		// hide pan zoom
		panZoomControlVisible: false 
	};
        
    vis.draw(draw_options,{network: networ_json});
        
        
    });
	
	// Helper functions
	
	function checkLength(o, n, min, max, tc) {
		if (o.val().length > max || o.val().length < min) {
			o.addClass("ui-state-error");
			o.focus();
			updateTips("Field <i>"+n+"</i> ranges from " +
				min + " to " + max + " symbols.",tc);
			return false;
		} else {
			return true;
		}
	}
	
	function checkRegexp(o, regexp, n,tc) {
		if (!(regexp.test(o.val()))) {
			o.addClass("ui-state-error");
			updateTips(n,tc);
			o.focus();
			return false;
		} else {
			return true;
		}
	}
	function updateTips(t,tc) {
		tc.html(t)
			.addClass("ui-state-highlight");
		setTimeout(function() {
			tc.removeClass("ui-state-highlight", 1500);
		}, 500);
	}
	
	function showMessage(title, msg, min_w, text_but) {
	
		$("#alert_d").get(0).title = title;
		$("#alert_d").get(0).innerHTML = msg;
		$( "#alert_d" ).dialog({modal: true, minWidth: min_w, buttons:[{text: text_but,
		click: function(){$(this).dialog("close");$(this).dialog("destroy");}}]});
	}
	function createTableGraph() {
		alert('Create table');
		var head_table = '<div class="row head_table">';
    	head_table += '<div class="col1">Group name</div>';
        head_table += '<div class="col2">Nodes</div>';
        head_table += '<div class="col3">Edges</div>';
    	head_table += '</div>';
		$("#graph").html(head_table);
		var row = '';
		for(var i=0; i<10; i++){
			row += '<div class="row">';
    		row += '<div class="col1">1</div>';
        	row += '<div class="col2">2</div>';
        	row += '<div class="col3">3</div>';
			row += '</div>';
			$("#graph").append(row);
		}
	}
</script>

</head>

<body>
<div id="cytoscapeweb">
    Cytoscape Web will replace the contents of this div with your graph.
</div>
<div id="toolbar" >
<button id="set_defaut_btn" >Reset the colouring</button>&nbsp;
<button id="save_graph_btn" >Save colouring</button>&nbsp;

</div>
<div style="padding:0 0 10px 10px;">
<div class="scroll">
<div id = "graph">
	
</div>
</div>
</div>

<!--  Dialog window -->
<div id="alert_d" title="Basic dialog"><p>
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    Your files have downloaded successfully into the My Downloads folder.
	</p>
</div>
<div class="ui-dialog" id="save_graph_wnd" title="Заголовок">
    <div class="validateTips ui-corner-all" id="tips_vp">&nbsp;</div>
    <div id="cont_form1">
    <form name="save_graph_form" id="save_graph_form" method="post" enctype="application/x-www-form-urlencoded">
    <div>
        <label for="file_name_ti"><b>File name</b></label>
        <input type="text" name="file_name_ti" id="file_name_ti" value="" class="text ui-widget-content ui-corner-all" />
    </div>
    </form>
    </div>
</div>
<div class="ui-dialog" id="set_par_graph_wnd" title="Заголовок">
    <div class="validateTips ui-corner-all" id="tips_vp">&nbsp;</div>
    <div id="cont_form1">
    <form name="save_par_graph_form" id="save_par_graph_form" method="post" enctype="application/x-www-form-urlencoded">
    <div>
        <label for="color_ti"><b>Color selection</b></label>
        <div><input type="text" name="color_ti" id="color_ti"  value="#fff" style="float:left; background-color: rgb(255, 244, 103);" class="iColorPicker ui-widget-content ui-corner-all" />&nbsp;
      <img id="set_color_btn" src="images/color.png" width="16" height="16"></div>
    </div>
    <div>
        <label for="name_ti"><b>Group name</b></label>
        <input type="text" name="name_ti" id="name_ti" value="" class="text ui-widget-content ui-corner-all" />
    </div>
    </form>
    </div>
</div>
</body>
    
    
    
</html>