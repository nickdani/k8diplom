<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<meta name="description" content="description"/>
<meta name="keywords" content="keywords"/> 
<meta name="author" content="author"/> 
<title>Kate Shevchenko 2013</title>
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link href="css/common.css" rel="stylesheet" type="text/css">
<link href="css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css">
<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>

<?php
$file_name = explode("/", $_SERVER['PHP_SELF']);
if($file_name[count($file_name)-1] == 'calc_cc.php'){
	echo '<script src="js/calc_cc.js"></script>';
}
if($file_name[count($file_name)-1] == 'factors.php'){
	echo '<script src="js/factor.js"></script>';
}
if($file_name[count($file_name)-1] == 'create_provider.php'){
	echo '<script src="js/create_provider.js"></script>';
}
if($file_name[count($file_name)-1] == 'compare_fact.php'){
	echo '<script src="js/compare_fact.js"></script>';
}
if($file_name[count($file_name)-1] == 'calc_cf.php'){
	echo '<script src="js/calc_cf.js"></script>';
}
if($file_name[count($file_name)-1] == 'calc_range_mark_pr.php'){
	echo '<script src="js/calc_range_mark_pr.js"></script>';
}
?>
</head>
<?php 
error_reporting(0);
	include_once ("conf.php");
	
	$start_time= microtime();
	$start_array = explode(" ",$start_time);
	$start_time = $start_array[1] + $start_array[0];
	
?>
<body>

<div id="layout_wrapper">
<div id="layout_edgetop"></div>

<div id="layout_container">

	<div id="site_title">

		<h1 class="left"><a href="#">Automated system for the alternatives selection </a></h1>
		<h2 class="right"></h2>

		<div class="clearer">&nbsp;</div>

	</div>

<div id="top_separator"></div>