<?php 
//error_reporting(0);
include_once("headgr_prv.php");
if($_SESSION['is_admin']){$isadmin = true;} else {$isadmin = false;}

?>

<div id="navigation">
    <div id="tabs">
        <ul>
            <li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            	<li><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
        </ul>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="spacer h5"></div>
<div id="main">
	<div class="left" id="main_left">
		<div id="main_left_content">
        	 <div class="post_title"><h1 style="font-size:16px; font-weight:300;">Create provider</h1></div>
        	<div style="border:1px solid #c6d2db; padding:1px; margin-top:5px; margin-bottom:5px;" >
            <form id="alt_form" name="alt_form" action="" enctype="application/x-www-form-urlencoded">
            <div id="cont_error" style="padding:5px; background-color:#FFC; margin:5px; border:1px solid red;">Error</div>
            <table cellpadding="5" cellspacing="2" border="0" width="100%">
            <tr bgcolor="#c6d2db">
                <td colspan="6" align="center" height="30">Name provider <input type="text" id="name_provider" style="width:180px">&nbsp;&nbsp;&nbsp;
                Comment <input type="text" id="comment" style="width:500px"></td>
            </tr>
            <tr bgcolor="#dae3e9">
                <td width="3%" align="center" height="25">N</td>
                <td width="40%" align="center" height="25">Factor</td>
                <td width="7%" align="center">K</td>
                <td width="3%" align="center">N</td>
                <td width="40%" align="center" height="25">Factor</td>
                <td width="7%" align="center">K</td>
            </tr>
            <tr align="center">
            	<td height="30">1</td>
                <td><input maxlength="50" class="t2" type="text" id="que1"></td>
                <td><input maxlength="50" class="t1" type="text" id="k1"></td>
                <td>9</td>
                <td><input maxlength="50" class="t2" type="text" id="que9"></td>
                <td><input maxlength="50" class="t1" type="text" id="k9"></td>
            </tr>
            <tr align="center"  bgcolor="#f5f5f5">
            	<td height="30">2</td>
                <td><input maxlength="50" class="t2" type="text" id="que2"></td>
                <td><input maxlength="50" class="t1" type="text" id="k2"></td>
                <td>10</td>
                <td><input maxlength="50" class="t2" type="text" id="que10"></td>
                <td><input maxlength="50" class="t1" type="text" id="k10"></td>
            </tr>
            <tr align="center">
            	<td height="30">3</td>
                <td><input maxlength="50" class="t2" type="text" id="que3"></td>
                <td><input maxlength="50" class="t1" type="text" id="k3"></td>
                <td>11</td>
                <td><input maxlength="50" class="t2" type="text" id="que11"></td>
                <td><input maxlength="50" class="t1" type="text" id="k11"></td>
            </tr>
            <tr align="center" bgcolor="#f5f5f5">
            	<td height="30">4</td>
                <td><input maxlength="50" class="t2" type="text" id="que4"></td>
                <td><input maxlength="50" class="t1" type="text" id="k4"></td>
                <td>12</td>
                <td><input maxlength="50" class="t2" type="text" id="que12"></td>
                <td><input maxlength="50" class="t1" type="text" id="k12"></td>
            </tr>
            <tr align="center">
            	<td height="30">5</td>
                <td><input maxlength="50" class="t2" type="text" id="que5"></td>
                <td><input maxlength="50" class="t1" type="text" id="k5"></td>
                <td>13</td>
                <td><input maxlength="50" class="t2" type="text" id="que13"></td>
                <td><input maxlength="50" class="t1" type="text" id="k13"></td>
            </tr>
            <tr align="center" bgcolor="#f5f5f5">
            	<td height="30">6</td>
                <td><input maxlength="50" class="t2" type="text" id="que6"></td>
                <td><input maxlength="50" class="t1" type="text" id="k6"></td>
                <td>14</td>
                <td><input maxlength="50" class="t2" type="text" id="que14"></td>
                <td><input maxlength="50" class="t1" type="text" id="k14"></td>
            </tr>
            <tr align="center">
            	<td height="30">7</td>
                <td><input maxlength="50" class="t2" type="text" id="que7"></td>
                <td><input maxlength="50" class="t1" type="text" id="k7"></td>
                <td>15</td>
                <td><input maxlength="50" class="t2" type="text" id="que15"></td>
                <td><input maxlength="50" class="t1" type="text" id="k15"></td>
            </tr>
            <tr align="center" bgcolor="#f5f5f5">
            	<td height="30">8</td>
                <td><input maxlength="50" class="t2" type="text" id="que8"></td>
                <td><input maxlength="50" class="t1" type="text" id="k8"></td>
                <td>16</td>
                <td><input maxlength="50" class="t2" type="text" id="que16"></td>
                <td><input maxlength="50" class="t1" type="text" id="k16"></td>
            </tr>
            
            <tr>
                <td colspan="6" align="center" height="40"><button id="save_provider_btn" >Save provider</button></td>
            </tr>
            </table>
            </form>
            </div>

		</div>
	</div>
</div>       
 
<!--  Dialog window -->
<div id="alert_d" title="Basic dialog"><p>
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    Your files have downloaded successfully into the My Downloads folder.
	</p>
</div>

<div class="ui-dialog" id="set_par_graph_wnd" title="Header">
    <div class="validateTips ui-corner-all" id="tips_vp">&nbsp;</div>
    <div id="cont_form1">
    <form name="save_par_graph_form" id="save_par_graph_form" method="post" enctype="application/x-www-form-urlencoded">
    <div>
        <label for="color_ti"><b>Color selection</b></label>
        <div><input type="text" name="color_ti" id="color_ti"  value="#fff" style="float:left; background-color: rgb(255, 244, 103);" class="iColorPicker ui-widget-content ui-corner-all" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
        <label for="name_ti"><b>Group name</b></label>
        <input type="text" name="name_ti" id="name_ti" value="" class="text ui-widget-content ui-corner-all" />
    </div>
    </form>
    </div>
</div>

<?php 
include_once("end.php");
?>