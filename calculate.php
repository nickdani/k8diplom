<?php
include_once("head.php");
?>

<div id="navigation">
<script type="text/javascript">
var count_str = 0;
$(function() {
	$("#calc_btn").click(function(e) {
		if($("#n_str").val() > 1) { $("#sel_str_form").get(0).submit(); }
		else { alert('Select at least 2 strategies');}
	});
	$("input[type=checkbox]").click(function(e) {
		if(e.target.checked){
			count_str = count_str+1;
		} else {
			count_str = count_str-1;
		}
		$("#n_str").val(count_str);
			
	});
	
});
</script>
    <div id="tabs">

        <ul>
            <li><a href="provider.php"><span>Providers</span></a></li>
            <li><a href="strategy.php"><span>Strategies</span></a></li>
            <li><a href="news.php"><span>New views</span></a></li>
            <li><a href="expert_list.php"><span>Experts</span></a></li>
            <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>

        </ul>

        <div class="clearer">&nbsp;</div>

    </div>

</div>

<div class="spacer h5"></div>

<div id="main">

    <div class="left" id="main_left">

        <div id="main_left_content">

            <div class="post">

                <div class="post_title">
                    <h1>Adding of new stratagy</h1>
                </div>

                <div class="post_body">
                    <?php
                    //include_once ("conf.php");
                    if (isSet($_SESSION['id_expert'])) {
						$st = new StatisticsView();
						echo $st -> getCalculateStrategy();  

                    } else {
                        print("<h2>You haven't logged in!</h2>");
                    }
                    ?>
                </div>

            </div>

        </div>

    </div>

    <?php
                    include_once("end.php");
    ?>