<?php

class EstimationOpinion {

    function setConsentLevel($id_opinion) {

        $query = "SELECT mark FROM estimation_opinion WHERE id_opinion='$id_opinion'";
        $result = mysql_query($query);
        $i = 0;
        $sum = 0;
        $res = 0;
        while ($name_row = mysql_fetch_row($result)) {
            $sum = $sum + $name_row[0];
            $i = $i + 1;
        }
        if ($i != 0) {
            $res = $sum / $i;
        }
        $query2 = "UPDATE opinion SET consent_level=$res WHERE id_opinion=$id_opinion";
        mysql_query($query2);

        $query3 = "SELECT id_expert FROM opinion WHERE id_opinion=$id_opinion";
        $result3 = mysql_query($query3);
        $id_expert;
        while ($name_row2 = mysql_fetch_row($result3)) {
            $id_expert = $name_row2[0];
        }
        $query4 = "SELECT mark FROM estimation_opinion INNER JOIN opinion ON
			`estimation_opinion`.`id_opinion`=`opinion`.`id_opinion`
            INNER JOIN expert ON opinion.`id_expert`=expert.`id_expert`
            WHERE opinion.id_expert=$id_expert";
        $result4 = mysql_query($query4);
        $sum2 = 0;
        $j = 0;
        $res2 = 0;
        while ($name_row4 = mysql_fetch_row($result4)) {
            $sum2 = $sum2 + $name_row4[0];
            $j = $j + 1;
        }
        if ($j != 0) {
            $res2 = $sum2 / $j;
        }

        $query5 = "SELECT id_interrogation FROM opinion WHERE id_opinion=$id_opinion";
        $result5 = mysql_query($query5);
        $id_interrogation;
        while ($name_row5 = mysql_fetch_row($result5)) {
            $id_interrogation = $name_row5[0];
        }
        $query7 = "SELECT * FROM interrogation_expert WHERE id_expert=$id_expert AND id_interrogation=$id_interrogation";
        $result7 = mysql_query($query7);

        $var = 0;
        while ($name_row7 = mysql_fetch_row($result7)) {
            $var = $name_row7[0];
        }

        if ($var != 0) {
            $query6 = "UPDATE interrogation_expert SET trust_level=$res2 WHERE id_expert=$id_expert
			AND id_interrogation=$id_interrogation";
            mysql_query($query6);
        } else {
            $query8 = "INSERT INTO interrogation_expert(id_expert,id_interrogation, trust_level) VALUES
				($id_expert,$id_interrogation ,$res2)";
            mysql_query($query8);
        }
    }

    function insertEstimationOpinion($id_expert, $id_opinion, $m, $comment) {
        $mark;
        switch ($m) {
            case -2:
                $mark = -2;
                break;
            case -1:
                $mark = -1;
                break;
            case 0:
                $mark = 0;
                break;
            case +1:
                $mark = 1;
                break;
            case +2:
                $mark = 2;
                break;
        }
        $query = "INSERT INTO estimation_opinion(id_expert, id_opinion, mark, comment)
			VALUES ('$id_expert','$id_opinion','$mark', '$comment')";
        mysql_query($query);
        $i = new EstimationOpinion();
        $i->setConsentLevel($id_opinion);
        //setConsentLevel($id_opinion);
    }

    function isOtherExpert($id_opinion, $id_est_expert) {
        $query = "SELECT id_expert FROM opinion WHERE id_opinion='$id_opinion'";
        $result = mysql_query($query);
        $id;
        while ($name_row = mysql_fetch_row($result)) {
            $id = $name_row[0];
        }
        if ($id == $id_est_expert) {
            return true;
        }
        else
            return false;
    }

    function hasEstimationOpinion($id_expert, $id_opinion) {
        $query = "SELECT COUNT(*) FROM `estimation_opinion` WHERE id_opinion=$id_opinion AND id_expert=$id_expert";
        $result = mysql_query($query);
        $count;

        while ($name_row = mysql_fetch_row($result)) {
            $count = $name_row[0];
        }

        return $count;
    }

}

?>