<?php

function cmp($a, $b) {
    if ($a['val'] == $b['val']) {
        return 0;
    }
    return ($a['val'] > $b['val']) ? -1 : 1;
}

class Statistics {

    function getInterrogationStatistics($type) {

        if ($type == "strategy") {
            $query = "SELECT o.preference_degree, e.trust_level, o.id_interrogation, i.subjects
FROM opinion o
LEFT JOIN strategy_expert e ON o.id_expert = e.id_expert
LEFT JOIN strategy i ON i.id_strategy = o.id_interrogation
WHERE o.type ='$type'";
        } else {

            $query = "select o.preference_degree ,
                        e.trust_level,
                        o.id_interrogation,
                        i.subjects
        from opinion o left join interrogation_expert e on o.id_expert = e.id_expert
        left join interrogation i on i.id_interrogation =o.id_interrogation where o.type='$type'";
        }
        $result = mysql_query($query);
        $return = array();

        while ($name_row = mysql_fetch_object($result)) {

            $val = 0;

//            var_dump($name_row);
//            die;

            if (!is_null($name_row->trust_level)) {
                $val = $name_row->trust_level * $name_row->preference_degree;
            } else {
                $val = $name_row->preference_degree;
            }

            if (isset($return[$name_row->id_interrogation])) {
                $return[$name_row->id_interrogation]['val'] += (double) $val;
            } else {
                $return[$name_row->id_interrogation] = array();
                $return[$name_row->id_interrogation]['subject'] = $name_row->subjects;
                $return[$name_row->id_interrogation]['val'] = (double) $val;
            }
        }

        uasort($return, "cmp");
//        var_dump($return);
//        die;

        return $return;

//        $query = "SELECT id_interrogation,subjects, COUNT(*) AS counts  FROM interrogation INNER JOIN opinion ON
//(opinion.`id_alternative`=`interrogation`.`id_alternative_A` OR
//opinion.`id_alternative`=`interrogation`.`id_alternative_B`) GROUP BY interrogation.`subjects`
//ORDER BY counts DESC";
//        $result = mysql_query($query);
//        $my_arr[][] = array();
//        $i = 0;
//        while ($name_row = mysql_fetch_row($result)) {
//            $my_arr[$i][0] = $name_row[0];
//            $my_arr[$i][1] = $name_row[1];
//            $my_arr[$i][2] = $name_row[2];
//            $i = $i + 1;
//        }
//        return $my_arr;
    }

    function getExpertStatisticsOnExperience() {
        $query = "SELECT login, id_expert, experience FROM expert ORDER BY experience DESC";
        $result = mysql_query($query);
        $my_arr[][] = array();
        $i = 0;
        while ($name_row = mysql_fetch_row($result)) {
            $my_arr[$i][0] = $name_row[0];
            $my_arr[$i][1] = $name_row[1];
            $my_arr[$i][2] = $name_row[2];
            $i = $i + 1;
        }
        return $my_arr;
    }

    function getExpertStatisticsOnTrustLevel($id_interrogation) {
        $query = "SELECT `expert`.`login`, expert.`id_expert`, interrogation_expert.`trust_level`
FROM expert INNER JOIN interrogation_expert 
ON expert.`id_expert`=`interrogation_expert`.`id_expert` WHERE id_interrogation=$id_interrogation ORDER BY interrogation_expert.`trust_level` DESC";
        $result = mysql_query($query);
        $my_arr[][] = array();
        $i = 0;
        while ($name_row = mysql_fetch_row($result)) {
            $my_arr[$i][0] = $name_row[0];
            $my_arr[$i][1] = $name_row[1];
            $my_arr[$i][2] = $name_row[2];
            $i = $i + 1;
        }
        return $my_arr;
    }
	
	function calculationSatatistic() {
		
		$num_str = $_POST['n_str'];
		$arr_str = array();
		$k = 0;
		$str_marks = array();
		
		// ��������� ������ ID ���������
		for($i = 1; $i<= $num_str; $i++){
			$id_str = 'str'.$i;
			if(!empty($_POST[$id_str])){
				$arr_str[$k] = $_POST[$id_str];
				$k++;
			}
		}
		
		
		$imax = count($arr_str);
		for($i=0; $i<$imax; $i++){
			//echo 'Strategy = '.$arr_str[$i];
			//�������� ������������ ������� �� ������� ����
			$query = "SELECT KV, KV_1, KV_2, KV_3, KV_4, POSSIBL, WEAK, STRONG, RISK FROM discricript_strategy 
	WHERE ID_STR = $arr_str[$i]";
			$result = mysql_query($query);
			$kv_arr =  array();
			//$i = 0;
			while ($name_row = mysql_fetch_row($result)) {
				if($name_row[5] == 1){
					//KV_P
					$kp_arr = array();
					$kp_arr[0] = $name_row[0];
					$kp_arr[1] = $name_row[1];
					$kp_arr[2] = $name_row[2];
					$kp_arr[3] = $name_row[3];
					$kp_arr[4] = $name_row[4];
					$kv_arr['pos'] = $kp_arr;
				}
				if($name_row[6] == 1){
					//KV_W
					$kw_arr = array();
					$kw_arr[0] = $name_row[0];
					$kw_arr[1] = $name_row[1];
					$kw_arr[2] = $name_row[2];
					$kw_arr[3] = $name_row[3];
					$kw_arr[4] = $name_row[4];
					$kv_arr['weak'] = $kw_arr;
				}
				if($name_row[7] == 1){
					//KV_S
					$ks_arr = array();
					$ks_arr[0] = $name_row[0];
					$ks_arr[1] = $name_row[1];
					$ks_arr[2] = $name_row[2];
					$ks_arr[3] = $name_row[3];
					$ks_arr[4] = $name_row[4];
					$kv_arr['strong'] = $ks_arr;
				}
				if($name_row[8] == 1){
					//KV_R
					$kr_arr = array();
					$kr_arr[0] = $name_row[0];
					$kr_arr[1] = $name_row[1];
					$kr_arr[2] = $name_row[2];
					$kr_arr[3] = $name_row[3];
					$kr_arr[4] = $name_row[4];
					$kv_arr['risk'] = $kr_arr;
				}
				
			}
			//echo '<pre>';
			//print_r($kv_arr);
			//echo '</pre>';
			// ������� ��������� ������� ��������� ������ ���������
			$query1 ="SELECT distinct  id_exp, id_str FROM `marks` WHERE id_str = $arr_str[$i]";
			$result1 = mysql_query($query1);
			$expert_arr = array();
			while ($name_row1 = mysql_fetch_row($result1)) {
				// ������ ���������
				$query2 = "SELECT m.mark, m.n_text FROM marks as m ";
				$query2 .= "LEFT JOIN discricript_strategy as ds ON ds.idds=m.id_ds ";
				$query2 .= "WHERE m.id_str=$arr_str[$i] and m.id_exp=$name_row1[0] AND ds.strong=1";
				//echo '������� ��������� = '.$name_row1[0];
				$result2 = mysql_query($query2);
				$ksm1 = $ksm2 = $ksm3 = $ksm4 = 0;
				while ($name_row2 = mysql_fetch_row($result2)) {
					// ������ ���������
					if($name_row2[1] == 1) {
						$ksm1 = $name_row2[0]*$kv_arr['strong'][1];
					}
					if($name_row2[1] == 2) {
						$ksm2 = $name_row2[0]*$kv_arr['strong'][2];
					}
					if($name_row2[1] == 3) {
						$ksm3 = $name_row2[0]*$kv_arr['strong'][3];
					}
					if($name_row2[1] == 4) {
						$ksm4 = $name_row2[0]*$kv_arr['strong'][4];
					}
					
				}
				$ks = ($ksm1+$ksm2+$ksm3+$ksm4)*$kv_arr['strong'][0];
				
				$query2 = "SELECT m.mark, m.n_text FROM marks as m ";
				$query2 .= "LEFT JOIN discricript_strategy as ds ON ds.idds=m.id_ds ";
				$query2 .= "WHERE m.id_str=$arr_str[$i] and m.id_exp=$name_row1[0] AND ds.weak =1";
			
				$result2 = mysql_query($query2);
				$kwm1 = $kwm2 = $kwm3 = $kwm4 = 0;
				while ($name_row2 = mysql_fetch_row($result2)) {
					// ������ ���������
					if($name_row2[1] == 1) {
						$kwm1 = $name_row2[0]*$kv_arr['weak'][1];
					}
					if($name_row2[1] == 2) {
						$kwm2 = $name_row2[0]*$kv_arr['weak'][2];
					}
					if($name_row2[1] == 3) {
						$kwm3 = $name_row2[0]*$kv_arr['weak'][3];
					}
					if($name_row2[1] == 4) {
						$kwm4 = $name_row2[0]*$kv_arr['weak'][4];
					}
					
				}
				$kw = ($kwm1+$kwm2+$kwm3+$kwm4)*$kv_arr['weak'][0];
				
				$query2 = "SELECT m.mark, m.n_text FROM marks as m ";
				$query2 .= "LEFT JOIN discricript_strategy as ds ON ds.idds=m.id_ds ";
				$query2 .= "WHERE m.id_str=$arr_str[$i] and m.id_exp=$name_row1[0] AND ds.possibl =1";
			
				$result2 = mysql_query($query2);
				$kpm1 = $kpm2 = $kpm3 = $kpm4 = 0;
				while ($name_row2 = mysql_fetch_row($result2)) {
					// ������ ���������
					if($name_row2[1] == 1) {
						$kpm1 = $name_row2[0]*$kv_arr['pos'][1];
					}
					if($name_row2[1] == 2) {
						$kpm2 = $name_row2[0]*$kv_arr['pos'][2];
					}
					if($name_row2[1] == 3) {
						$kpm3 = $name_row2[0]*$kv_arr['pos'][3];
					}
					if($name_row2[1] == 4) {
						$kpm4 = $name_row2[0]*$kv_arr['pos'][4];
					}
					
				}
				$kp = ($kpm1+$kpm2+$kpm3+$kpm4)*$kv_arr['pos'][0];
				
				$query2 = "SELECT m.mark, m.n_text FROM marks as m ";
				$query2 .= "LEFT JOIN discricript_strategy as ds ON ds.idds=m.id_ds ";
				$query2 .= "WHERE m.id_str=$arr_str[$i] and m.id_exp=$name_row1[0] AND ds.risk =1";
			
				$result2 = mysql_query($query2);
				$krm1 = $krm2 = $krm3 = $krm4 = 0;
				while ($name_row2 = mysql_fetch_row($result2)) {
					// ������ ���������
					if($name_row2[1] == 1) {
						$krm1 = $name_row2[0]*$kv_arr['risk'][1];
					}
					if($name_row2[1] == 2) {
						$krm2 = $name_row2[0]*$kv_arr['risk'][2];
					}
					if($name_row2[1] == 3) {
						$krm3 = $name_row2[0]*$kv_arr['risk'][3];
					}
					if($name_row2[1] == 4) {
						$krm4 = $name_row2[0]*$kv_arr['risk'][4];
					}
				}
				$kr = ($krm1+$krm2+$krm3+$krm4)*$kv_arr['risk'][0];
				$expert_arr[$name_row1[0]] = $ks+$kw+$kp+$kr;
			}
			$str_marks[$arr_str[$i]] = $expert_arr;
		}
		//echo '<pre>';
		//print_r($str_marks);
		//echo '</pre>';
		// ������ ������������ �������������
		$sv = 0;
		$v = 0;
		$sum_aj = 0;
		
		foreach ($str_marks as $key => $value) {
    		//echo "Key: $key; Value: ".count($value)."<br />\n";
			  
			$n_exp = count($value);
			//$sv = 0;
			$sum_aj = 0;
			foreach ($value as $key2 => $value2){
				$sum_aj += $value2/($n_exp);/// ���� ��� $sum_aj += $value2 � ������ ����������
				//$sum_aj += $value2;

			}
			$sv += ($sum_aj-(($num_str+1)*$n_exp)/2)*($sum_aj-(($num_str+1)*$n_exp)/2);
		}
		$v = (12 * $sv)/(($n_exp*$n_exp*($num_str*$num_str*$num_str-$num_str)));
		//echo 'V = '.$v;
		
		// ������ �������������� ������
		
		$agr_m = array();
		foreach ($str_marks as $key => $value) {
			$sum_agm = 0;
			foreach ($value as $key2 => $value2){
				$sum_agm += $value2;
			}
			$agr_m[$key] = $sum_agm;
		}
		//print_r($agr_m);
		
		// ����� �����������

		arsort($agr_m);
		foreach ($agr_m as $key => $value) {
			$query = "SELECT id_strategy, subjects FROM strategy WHERE id_strategy = $key";
			$result = mysql_query($query);
			$ret_str = '<li class="alt">';
			while ($name_row = mysql_fetch_row($result)) {
				$ret_str .= '<div class="archive_title"><a href="interrogation.php?i_id='.$key.'"> '.$name_row[1].' </a></div>';
				$ret_str .= '<div class="date">Ranged marks:'.$agr_m[$key].'</div>';
			}
			$ret_str .= '</li>';
			echo $ret_str;
		}
		
	}

}

?>