// JavaScript Document
$(function() {
	// hide dialog window
	$("#save_graph_wnd").hide(); 
	$("#alert_d").hide();
	$("#cont_error").hide();
	
	// get data
	var post = {};
	post.act = 'getcmp';
	$.post('get_factors_pr.php', post, function(data, status) {
		if(status == 'success'){
			$("#fact_cont").append(data);
		} else {
			// Server error
			showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
		}
	});

	//Tool bar
	$("#save_provider_btn").button({icons:{primary:"ui-icon-disk"},text:true});
	
	$("#save_provider_btn").click(function(e) {
		e.preventDefault();
		alert(this.id);
		// Checking input fields
		 var reg_exp1 = /^[0-9a-zA-Z�-��-߳�����\-\(\)\. ]{3,}$/i;
		 var reg_exp2 = /^[0-9]{1,}\.{0,1}[0-9]{1,}$/i;
		 var text_msg1 = ' only latin letter and number are allowed!'
		 var text_msg2 = ' only float number are allowed!'
		 
		if(checkLength2($("#name_provider"), 'Name provider', 3, 50) === false){return;}
		if(checkRegexp2($("#name_provider"), reg_exp1, 'Name provider', text_msg1 ) === false){return;}
		
		if(checkLength2($("#ex_data"), 'Extra data', 3, 50) === false){return;}
		if(checkRegexp2($("#ex_data"), reg_exp1, 'Extra data', text_msg1 ) === false){return;}
		
		
		var post = {};
		post.name_provider = $("#name_provider").val();
		post.ex_data = $("#ex_data").val();
		post.type = $("#type_pr").val();
		// mark provider
		post.mark1 = $("#mark1").val();
		post.mark2 = $("#mark2").val();
		post.mark3 = $("#mark3").val();
		post.mark4 = $("#mark4").val();
		post.mark5 = $("#mark5").val();
		post.mark6 = $("#mark6").val();
		post.mark7 = $("#mark7").val();
		post.mark8 = $("#mark8").val();
		post.mark9 = $("#mark9").val();
		post.mark10 = $("#mark10").val();
		post.mark11 = $("#mark11").val();
		
		var url = 'save_provider.php';
		$.post(url, post, function(data, status) {
			//alert(status);
			if(status == 'success'){
				//image_name = 'none'
				showMessage('Message', '<p>'+data+'</p>', 300, "Close");
			} else {
				// Server error
				showMessage('Message', '<p>File does not save! Server error.</p>'+data, 400, "Close");
			}
		});
	});
	
	var file_name = $("#file_name_ti");
	// ����� ��������� ��� �������� ���������
	var vpFields = $([]).add(file_name);
	// Dialog window
	var tips_vp = $("#tips_vp");
	
});

// Helper functions

function checkLength2(o, n, min, max) {
	var error = $("#cont_error");
	if (o.val().length > max || o.val().length < min) {
		error.get(0).innerHTML = "Field <i><b>"+n+"</b></i> ranges from " + min + " to " + max + " symbols.";
		error.show();
		o.focus();
		return false;
	} else {
		error.hide();
		return true;
	}
}
function checkRegexp2(o, regexp, n, text_error) {
	//console.info(o);
	
	var error = $("#cont_error");
	if (!(regexp.test(o.val()))) {
		error.get(0).innerHTML = "In field <i><b>"+n+"</b></i> "+text_error;
		error.show();
		o.focus();
		return false;
	} else {
		error.hide();
		return true;
	}
}

function checkLength(o, n, min, max, tc) {
	if (o.val().length > max || o.val().length < min) {
		o.addClass("ui-state-error");
		o.focus();
		updateTips("Field <i>"+n+"</i> ranges from " +
			min + " to " + max + " symbols.",tc);
		return false;
	} else {
		return true;
	}
}

function checkRegexp(o, regexp, n,tc) {
	if (!(regexp.test(o.val()))) {
		o.addClass("ui-state-error");
		updateTips(n,tc);
		o.focus();
		return false;
	} else {
		return true;
	}
}
function updateTips(t,tc) {
	tc.html(t)
		.addClass("ui-state-highlight");
	setTimeout(function() {
		tc.removeClass("ui-state-highlight", 1500);
	}, 500);
}

function showMessage(title, msg, min_w, text_but) {

	$("#alert_d").get(0).title = title;
	$("#alert_d").get(0).innerHTML = msg;
	$( "#alert_d" ).dialog({modal: true, minWidth: min_w, buttons:[{text: text_but,
	click: function(){$(this).dialog("close");$(this).dialog("destroy");}}]});
}