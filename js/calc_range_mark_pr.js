// JavaScript Document
$(function() {
	// hide dialog window
	$("#alert_d").hide();
	$("#cont_error").hide();
	
	$("#calc_rmpr_btn").button({icons:{primary:"ui-icon-calculator"},text:true});
		
	$("#calc_rmpr_btn").click(function(e) {
		e.preventDefault();
		var url = 'calc_sum_mark_pr.php';
		var post = {};
		post.id_pr = $("#provider").val();
		post.provider = $("#provider").text();
		$.post(url, post, function(data, status) {
				if(status == 'success'){
					$("#result_mark").remove();
					$("#result_rmp").append(data);
				} else {
					// Server error
					showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
				}
		});
	});
	
});