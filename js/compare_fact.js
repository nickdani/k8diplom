// JavaScript Document
var debug_obj = {};
$(function() {
	// hide dialog window
	$("#alert_d").hide();
	$("#cont_error").hide();
	
	$("#save_compare_btn").button({icons:{primary:"ui-icon-disk"},text:true});
	// get data
	var post = {};
	post.act = 'getcmp';
	$.post('get_factors.php', post, function(data, status) {
		if(status == 'success'){
			$("#compare_fact_cont").append(data);
		} else {
			// Server error
			showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
		}
	});
	
	$("#save_compare_btn").click(function(e) {
		e.preventDefault();
		// save compare factories
		var reg = /idf/;
		var post = {};
		var url = 'save_comp_fact.php';
		var str_data = '';
		var nr = 0
		$.each( $("#comp_fact_tb tr"), function( key, value ) {
			debug_obj = value;
			id1 = debug_obj.cells[0].id.replace(reg,'');
			id2 = debug_obj.cells[2].id.replace(reg,'');
			idrow = debug_obj.id;
			mark1 = $("#mark1"+idrow).val();
			mark2 = $("#mark2"+idrow).val();
			mark = mark1-mark2;
			
			str_data += id1+':'+id2+":"+mark+";";
			str_data += id2+':'+id1+":"+(0-mark)+";";
			
		});
		var imax = $("#num_row").val();
		// �������� ������������ ��������
		for(var i=1; i<=imax; i++){
			str_data += i+':'+i+":0;";
		}
		str_data = str_data.substr(0, (str_data.length-1));
		post.data_comp = str_data;

		$.post(url, post, function(data, status) {
				if(status == 'success'){
					if(data == 'ok'){
						showMessage('Message', '<p>Data saved successfully!</p>', 400, "Close");
					} else {
						showMessage('Message', '<p>Answer server: </p>'+data, 600, "Close");
					}
				} else {
					// Server error
					showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
				}
			});
	});
});

function showMessage(title, msg, min_w, text_but) {

	$("#alert_d").get(0).title = title;
	$("#alert_d").get(0).innerHTML = msg;
	$( "#alert_d" ).dialog({modal: true, minWidth: min_w, buttons:[{text: text_but,
	click: function(){$(this).dialog("close");$(this).dialog("destroy");}}]});
}