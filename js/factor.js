// JavaScript Document
$(function() {
	// hide dialog window
	$("#factor_wnd").hide(); 
	$("#alert_d").hide();
	$("#del_factor_wnd").hide();
	$("#cont_error").hide();
	// get data
	var post = {};
	post.act = 'view';
	$.post('do_factor.php', post, function(data, status) {
		//alert(status);
		if(status == 'success'){
			//image_name = 'none'
			$("#cont_factor").append(data);
			$("#fact_tb tr").bind('click', function(e) {
				$("#fact_tb tr").removeClass('sel_row');
				$("#"+this.id).addClass('sel_row');
			});
		} else {
			// Server error
			showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
		}
	});
	//Tool bar

	$("#create_factor_btn").button({icons:{primary:"ui-icon-document"},text:true});
	$("#edit_factor_btn").button({icons:{primary:"ui-icon-pencil"},text:true});
	$("#delete_factor_btn").button({icons:{primary:"ui-icon-trash"},text:true});
	
	$("#create_factor_btn").click(function(e) {
		e.preventDefault();
		//alert(this.id);
		showModalForm('create','Create factor','',0);
	});
	
	$("#edit_factor_btn").click(function(e) {
		e.preventDefault();
		if($(".sel_row").size() > 0) {
			var text_factor = $(".sel_row td")[1].innerHTML;
			var id = $(".sel_row").attr('id');
			var reg = /f/;
			showModalForm('edit','Edit factor',text_factor,id.replace(reg, ''));
		}
	});
	
	$("#delete_factor_btn").click(function(e) {
		e.preventDefault();
		if($(".sel_row").size() > 0) {
			var text_factor = $(".sel_row td")[1].innerHTML;
			var id = $(".sel_row").attr('id');
			var reg = /f/;
			showDeleteWnd(text_factor,id.replace(reg, ''));
		}
		
	});
	
	
	
});
	

// Helper functions

function showMessage(title, msg, min_w, text_but) {

	$("#alert_d").get(0).title = title;
	$("#alert_d").get(0).innerHTML = msg;
	$( "#alert_d" ).dialog({modal: true, minWidth: min_w, buttons:[{text: text_but,
	click: function(){$(this).dialog("close");$(this).dialog("destroy");}}]});
}

function showDeleteWnd(txt, id){
	
	$("#del_factor_wnd").get(0).title = 'Delete factor';
	$("#msg_fact")[0].innerHTML = 'Do you want to delete factor:<br><span style="color:red;">"'+txt+'"</span> ?';
	var post = {};
	post.idf = id;
	post.act = 'delete';
	
	$("#del_factor_wnd").dialog({modal: true, minWidth: 400, 
		buttons:[{text: 'Delete',
		click: function(){
			//alert('Delete factor');
			
			$.post('do_factor.php', post, function(data, status) {
				//alert(status);
				if(status == 'success'){
					$("#fact_tb tr").unbind('click');
					$("#cont_factor table").remove();
					$("#cont_factor").append(data);
					$("#fact_tb tr").bind('click', function(e) {
						$("#fact_tb tr").removeClass('sel_row');
						$("#"+this.id).addClass('sel_row');
					});
					$("#del_factor_wnd").dialog("close");
				} else {
					// Server error
					showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
				}
			});
			
			
			$(this).dialog("close");
			$(this).dialog("destroy");
		}},
		{text:'Cancel',
		click: function(){
			$(this).dialog("close");
			$(this).dialog("destroy");
		}}	
		]
	});
}

function showModalForm(act,title,text,id) {
	$("#factor_wnd").get(0).title = title;
	$("#fact_ti").val(text);
	$("#idf").val(id);
	var post = {};
	post.fact = text;
	post.idf = id;
	post.act = act;
	var url = 'do_factor.php';
	$("#factor_wnd").dialog({modal: true, minWidth: 400, 
		buttons:[{text: 'Save',
		click: function(){
			if(act == 'create' || act == 'edit'){
				post.fact = $("#fact_ti").val();
				
				$.post(url, post, function(data, status) {
					//alert(status);
					if(status == 'success'){
						//image_name = 'none'
						$("#fact_tb tr").unbind('click');
						$("#cont_factor table").remove();
						$("#cont_factor").append(data);
						$("#fact_tb tr").bind('click', function(e) {
							$("#fact_tb tr").removeClass('sel_row');
							$("#"+this.id).addClass('sel_row');
						});
						$("#factor_wnd").dialog("close");
						//showMessage('Message', '<p>'+data+'</p>', 300, "Close");
					} else {
						// Server error
						showMessage('Message', '<p>Server error.</p>'+data, 400, "Close");
					}
					
					$("#factor_wnd").dialog("destroy");
				});
			}
			
		}},
		{text:'Cancel',
		click: function(){
			$(this).dialog("close");
			$(this).dialog("destroy");
		}}	
		]
	});
}