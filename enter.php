<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
               <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Entering to the system</h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
					<div class="post_body">
						<p>Login:</p>
						<form  method="post" action="enter_action.php">
						  <label>
						  <input type="text" name="enter_log" class="styled" >
						  </label>
						
						<p>Password:</p>
						
						  <label>
						  <input type="password" name="enter_pass"  class="styled">
						  </label>
						
						<p>&nbsp;</p>
						
						  <label>
						  <input type="submit"  value="Enterring" />
						  </label>
						  <p>&nbsp;</p>
						  
						</form>
						</div>
						 </ol>
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>