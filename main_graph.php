<?php 
//error_reporting(0);
include_once("headgr.php");
if($_SESSION['is_admin']){$isadmin = true;} else {$isadmin = false;}

?>

<div id="navigation">
    <div id="tabs">
        <ul>
            <li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            	<li><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
        </ul>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="spacer h5"></div>
<div id="main">
	<div class="left" id="main_left">
		<div id="main_left_content">
        	 <div class="post_title"><h1 style="font-size:16px; font-weight:300;">Create strategy</h1></div>
        	<div style="border:1px solid #c6d2db; padding:1px; margin-top:5px; margin-bottom:5px;" >
            <form id="alt_form" name="alt_form" action="" enctype="application/x-www-form-urlencoded">
            <div id="cont_error" style="padding:5px; background-color:#FFC; margin:5px; border:1px solid red;">Error</div>
            <table cellpadding="5" cellspacing="2" border="0" width="100%">
            <tr bgcolor="#c6d2db">
                <td colspan="4" align="center" height="30">Name strategy <input type="text" id="name_stratage" style="width:180px">&nbsp;&nbsp;&nbsp;
                Comment <input type="text" id="comment" style="width:500px"></td>
            </tr>
            <tr bgcolor="#dae3e9">
                <td align="center" height="25">RISC &nbsp;&nbsp;&nbsp;&nbsp;( Krisc = <input class="t1" maxlength="5" type="text" id="k_risk"> )</td>
                <td align="center">K1-K4</td>
                <td align="center">STRONG &nbsp;&nbsp;&nbsp;&nbsp;( Kpositiv = <input maxlength="5" class="t1" type="text" id="k_pos"> )</td>
                <td align="center">K1-K4</td>
            </tr>
            <tr>
                <td>1 <input maxlength="50" class="t2" type="text" id="risk1"></td>
                <td>1 <input maxlength="50" class="t1" type="text" id="kr1"></td>
                <td>1 <input maxlength="50" class="t2" type="text" id="strong1"></td>
                <td>1 <input maxlength="50" class="t1" type="text" id="ks1"></td>
            </tr>
            <tr>
                <td>2 <input maxlength="50" class="t2" type="text" id="risk2"></td>
                <td>2 <input maxlength="50" class="t1" type="text" id="kr2"></td>
                <td>2 <input maxlength="50" class="t2" type="text" id="strong2"></td>
                <td>2 <input maxlength="50" class="t1" type="text" id="ks2"></td>
            </tr>
            <tr>
                <td>3 <input maxlength="50" class="t2" type="text" id="risk3"></td>
                <td>3 <input maxlength="50" class="t1" type="text" id="kr3"></td>
                <td>3 <input maxlength="50" class="t2" type="text" id="strong3"></td>
                <td>3 <input maxlength="50" class="t1" type="text" id="ks3"></td>
            </tr>
            <tr>
                <td>4  <input maxlength="50" class="t2" type="text" id="risk4"></td>
                <td>4 <input maxlength="50" class="t1" type="text" id="kr4"></td>
                <td>4 <input maxlength="50" class="t2" type="text" id="strong4"></td>
                <td>4 <input maxlength="50" class="t1" type="text" id="ks4"></td>
            </tr>
            <tr bgcolor="#dae3e9">
                <td align="center" height="25">POSSIBILITEIS ( Kpos = <input class="t1" maxlength="5" type="text" id="k_psb"> )</td>
                <td align="center">K1-K4</td>
                <td align="center">WEAK &nbsp;&nbsp;&nbsp;&nbsp;( Knegative = <input maxlength="5" class="t1" type="text" id="k_neg"> )</td>
                <td align="center">K1-K4</td>
            </tr>
            <tr>
                <td>1 <input maxlength="50" class="t2" type="text" id="psb1"></td>
                <td>1 <input maxlength="50" class="t1" type="text" id="kpb1"></td>
                <td>1 <input maxlength="50" class="t2" type="text" id="weak1"></td>
                <td>1 <input maxlength="50" class="t1" type="text" id="kw1"></td>
            </tr>
            <tr>
                <td>2 <input maxlength="50" class="t2" type="text" id="psb2"></td>
                <td>2 <input maxlength="50" class="t1" type="text" id="kpb2"></td>
                <td>2 <input maxlength="50" class="t2" type="text" id="weak2"></td>
                <td>2  <input maxlength="50" class="t1" type="text" id="kw2"></td>
            </tr>
            <tr>
                <td>3 <input maxlength="50" class="t2" type="text" id="psb3"></td>
                <td>3 <input maxlength="50" class="t1" type="text" id="kpb3"></td>
                <td>3 <input maxlength="50" class="t2" type="text" id="weak3"></td>
                <td>3 <input maxlength="50" class="t1" type="text" id="kw3"></td>
            </tr>
            <tr>
                <td>4 <input maxlength="50" class="t2" type="text" id="psb4"></td>
                <td>4 <input maxlength="50" class="t1" type="text" id="kpb4"></td>
                <td>4 <input maxlength="50" class="t2" type="text" id="weak4"></td>
                <td>4 <input maxlength="50" class="t1" type="text" id="kw4"></td>
            </tr>
            <tr>
                <td colspan="4" align="center" height="40"><button id="save_stratage_btn" >Save stratagy</button></td>
            </tr>
            </table>
            </form>
            </div>
        		
            <div class="post_title" style=" margin-bottom:5px;"><h1 style="font-size:16px; font-weight:300;">Graph building</h1></div>
            <div id="cytoscapeweb" style="height:400px; margin-left:auto; margin-right:auto;">
                Cytoscape Web will replace the contents of this div with your graph.
            </div>
            <div id="toolbar" style="margin-left:auto; margin-right:auto;" >
            <button id="set_defaut_btn" >Reset the colouring</button>&nbsp;
            <button id="save_graph_btn" >Save colouring</button>&nbsp;
            <button id="save_png_btn" >Save to image</button>&nbsp;
            </div>
    
            <div class="scroll">
            	<div id = "graph"></div>
        	</div>
		</div>
	</div>
</div>       
 
<!--  Dialog window -->
<div id="alert_d" title="Basic dialog"><p>
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    Your files have downloaded successfully into the My Downloads folder.
	</p>
</div>
<div class="ui-dialog" id="save_graph_wnd" title="Заголовок">
    <div class="validateTips ui-corner-all" id="tips_vp">&nbsp;</div>
    <div id="cont_form1">
    <form name="save_graph_form" id="save_graph_form" method="post" enctype="application/x-www-form-urlencoded">
    <div>
        <label for="file_name_ti"><b>File name</b></label>
        <input type="text" name="file_name_ti" id="file_name_ti" value="" class="text ui-widget-content ui-corner-all" />
        <input type="hidden" id="act" value="">
        
    </div>
    </form>
    </div>
</div>
<div class="ui-dialog" id="set_par_graph_wnd" title="Header">
    <div class="validateTips ui-corner-all" id="tips_vp">&nbsp;</div>
    <div id="cont_form1">
    <form name="save_par_graph_form" id="save_par_graph_form" method="post" enctype="application/x-www-form-urlencoded">
    <div>
        <label for="color_ti"><b>Color selection</b></label>
        <div><input type="text" name="color_ti" id="color_ti"  value="#fff" style="float:left; background-color: rgb(255, 244, 103);" class="iColorPicker ui-widget-content ui-corner-all" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
        <label for="name_ti"><b>Group name</b></label>
        <input type="text" name="name_ti" id="name_ti" value="" class="text ui-widget-content ui-corner-all" />
    </div>
    </form>
    </div>
</div>

<?php 
include_once("end.php");
?>