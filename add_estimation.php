<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="index.php"><span>Providers</span></a></li>
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            <li><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Adding a mark to a view</h1>
					</div>
				
					<div class="post_body">
					
						<?php 
						
						if(isSet($_GET['i_id'])&&isSet($_GET['o_id']))
						{
							if(isSet($_SESSION['id_expert']))
						{
						$d=$_GET['i_id'];
						$s=$_GET['o_id'];
						$id_expert=$_SESSION['id_expert'];
						$est_op=new EstimationOpinion();
						$bool=$est_op->hasEstimationOpinion($id_expert,$s);
						
						if($bool==0)
						{
						print("<form  method=\"post\" action=\"estimation_action.php?int_id=$d&op_id=$s\">");
						
						 print(" <select name=\"jumpMenu_estimation\" onchange=\"MM_jumpMenu('parent',this,0)\">
						    <option value=\"-2\">don't agree</option>
						    <option value=\"-1\">probably don't agree</option>
						    <option selected=\"selected\" value=\"0\">neutral</option>
						    <option value=\"+1\">probably agree</option>
						    <option value=\"+2\">agree</option>
						  </select>
						
						   <BR>Comment:  <BR>
						  <label>
						  <textarea name=\"comments_estimation\"  cols=\"45\" rows=\"5\"></textarea>
						  </label>
						
						  <br>
						  <label>
						  <input type=\"submit\"  value=\"Add\" />
						  </label>");
						}
						else
						{
							print("<h2>You have already given a mark to this view</h2>");
						}
						 print("<p><a href = \"estimation.php?i_id=$d&o_id=$s\">Back to the expert opinion</a></p>");
						 //print("<p><a href = \"interrogation.php?i_id=$d\">Back to the pool</a></p>");
						 
						  print("</form>");
						}
						else
						{
							$d=$_GET['i_id'];
							$s=$_GET['o_id'];
							print("<h2>You haven't loged in!</h2><br>");
							// print("<p><a href = \"interrogation.php?i_id=$d\">Back to the pool</a></p>");
							 print("<p><a href = \"estimation.php?i_id=$d&o_id=$s\">Back to the experts opinion</a></p>");
						}
						}
						else
						{
							print("<h2>Error!</h2>");	
						}
						
						
						
						?>
					
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>