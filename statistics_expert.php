<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                 <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Experts statistics acording to the experience </h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
						<?php
							$statistics=new Statistics();
							$arr1=$statistics->getExpertStatisticsOnExperience();
							$st=new StatisticsView();
							$st->printExpertStatisticsOnExperience($arr1,10);
							
					   ?>
					 </ol>
					</div>
					<br>
					<div class="post_title">
						<h1>Experts statistics acording to the level of trust in pools</h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
						<?php
						if(isSet($_GET['page'])){
						$page=$_GET['page'];
						$onpage=10;
							$interrogation= new Interrogation();
							$arr=$interrogation->getList($page,$onpage);
							$count=$interrogation->getNumOfPages($onpage);
							$st=new StatisticsView();
							$st->printExpertStatisticsList($arr,$count);}
							else
					{
						print("<h2>Error!</h2>");
					}
							
					   ?>
					 </ol>
					</div>

				</div>
		
			</div>

		</div>

		
<?php 
include_once("end.php");
?>