<?php 
include_once("head.php");
?>


	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li ><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                 <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Result</h1>
					</div>
				
					<div class="post_body">
					<?php 
					if(isSet($_GET['i_id'])&&isSet($_GET['opinion_id']))
					{
					$d=$_GET['i_id'];
					$s=$_GET['opinion_id'];
					$opinion=new Opinion();
					$bool=$opinion->hasEstimationOpinion($s);
					if(!$bool)
					{
					
					print("<form  method=\"post\" action=\"update_estimation_action.php?int_id=$d&op_id=$s\">");
					
					  print("<select name=\"jumpMenu_update_estimation\" onchange=\"MM_jumpMenu('parent',this,0)\">");
					 
					  $opinion=new Opinion();
					  //print("S= $s <BR>");
					  $preference_degree=$opinion->getPreferenceDegree($s);
					 // print("$preference_degree  preference_degree");
					  switch($preference_degree)
								{
								case -3:
									print(" <option selected=\"selected\" value=\"-3\">totally against</option>
										    <option value=\"-2\">against</option>
										    <option value=\"-1\">probably against</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    <option value=\"+3\">totally agree</option>");
									break;
								case -2:
									print(" <option value=\"-3\"> totally against</option>
										    <option selected=\"selected\" value=\"-2\">against</option>
										    <option value=\"-1\"> against</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    <option value=\"+3\">totally agree</option>");
									break;
								case -1:
									print(" <option value=\"-3\">totally against</option>
										    <option value=\"-2\">against</option>
										    <option selected=\"selected\" value=\"-1\">probably against</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    <option value=\"+3\">totally agree</option>");
									break;
								case 0:
									print(" <option value=\"-3\">totally against</option>
										    <option value=\"-2\">against</option>
										    <option value=\"-1\">probably against</option>
										    <option  selected=\"selected\" value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    <option value=\"+3\">totally agree</option>");
									break;
								case 1:
									print(" <option value=\"-3\">totally against</option>
										    <option value=\"-2\">against</option>
										    <option value=\"-1\">probably against</option>
										    <option value=\"0\">neutral</option>
										    <option  selected=\"selected\" value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    <option value=\"+3\">totally agree</option>");
									break;
								case 2:
									print(" <option value=\"-3\">totally against</option>
										    <option value=\"-2\">against</option>
										    <option value=\"-1\">probably against</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option selected=\"selected\" value=\"+2\">agree</option>
										    <option value=\"+3\">totally agree</option>");
									break;
								case 3:
									print(" <option value=\"-3\">totally against</option>
										    <option value=\"-2\">against</option>
										    <option value=\"-1\">probably against</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    <option  selected=\"selected\" value=\"+3\">totally agree</option>");
									break;
								}
					 
					  print("</select>
					
					   <BR> Comment:  <BR>
					  <label>
					  <textarea name=\"comments_update_estimation\"  cols=\"45\" rows=\"5\">
					");
					$comments=$opinion->getComments($s);
					print($comments);
					//print(trim($comments));
					print("</textarea>
					  </label>
					
					  <br>
					  <label>
					  <input type=\"submit\"  value=\"Change\" />
					  </label>");
					  
					  print("</form>");
					  print("<BR><a href = \"estimation.php?i_id=$d&o_id=$s\"> Back<BR></a>");
					}
					else
					{
						print("<h2>You can't change the mark!</h2>
						<BR><a href = \"estimation.php?i_id=$d&o_id=$s\"> Back<BR></a>");
					}
					}
					else
					{
						print("<h2>Error!</h2>");
					}
					  ?>
					
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>