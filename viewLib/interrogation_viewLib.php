<?php 
class InterrogationView
{
	function displayList($ilist, $page_count)
	{
	$i=0;
	$count=count($ilist,0);
	 	while($i<$count-1)
		{
			$i_id = $ilist[$i][0];
			$i_name = stripcslashes($ilist[$i][1]);
			$date=$ilist[$i][2];
			$id_expert=$ilist[$i][3];
			$login=$ilist[$i][4];
			$link="<li class=\"alt\">

								<div class=\"archive_title\">
									<a href=\"interrogation.php?i_id=$i_id\"> $i_name</a>
								</div>
								<div class=\"date\">
								Created $date<br>
								Author: <a href=\"expert.php?id_expert=$id_expert\"> $login </a>
								</div>
							</li>";
			//$link="<li><a href = \"interrogation.php?i_id=$i_id\"> $i_name<BR></a>";
			print ($link);
			$i++;
		}
		print("<br> Page ");
		$i=1;
		while($i<$page_count+1)
		{
			if($i==$ilist['page'])
			{
				print(" -$i- ");
			}
			else
			{
				print("<a href=\"interrogation_list.php?page=$i\"> -$i- </a>");
			}
			$i=$i+1;
		}
	}
	
function displayNewList($ilist, $num)
	{
	$i=0;
	$count=count($ilist,0);
	 	while($i<$num)
		{
			if($i==$count)break;
			$i_id = $ilist[$i][0];
			if(isSet($ilist[$i][1]))
			{
			$i_name = stripcslashes($ilist[$i][1]);
			$date=$ilist[$i][2];
			$id_expert=$ilist[$i][3];
			$login=$ilist[$i][4];
			$link="<li class=\"alt\">

								<div class=\"archive_title\">
									<a href=\"interrogation.php?i_id=$i_id\"> $i_name</a>
								</div>
								<div class=\"date\">
								Created $date<br>
								Author: <a href=\"expert.php?id_expert=$id_expert\"> $login </a>
								</div>
							</li>";
			//$link="<li><a href = \"interrogation.php?i_id=$i_id\"> $i_name<BR></a>";
			print ($link);
			$i++;
			}
			else
			{
				print("No pools");
				return;
			}
		}
		print("<br> <a href=\"interrogation_list.php?page=2\">Next </a>");
	}
	
	function displayComments($comments)
	{
		print(stripcslashes($comments));
	}
	
	function printNameOfAlternative($name)
	{
		print(stripcslashes($name));
	}
	
	
	function printSubOfInterrogation($name,$fname,$textArr)
	{
		//print (stripcslashes($name));
		
		$block = '<table width="100%" border="1"><tr><td colspan="2">'.$name.'</td></tr>';
		$block .= '<tr><td><img src="data/images/'.$fname.'.png" /></td><td><table>';
		$imax = count($textArr);
		for ($i = 0; $i<$imax; $i++ )
		$block .= "<tr><td>&nbsp;</td><td>$textArr[$i]</td</tr>";
		
		//$block .= '<table width="600" cellpadding="5" cellspacing="10" border="0"><tr>';
		//$block .= '<td><input name="mark1" type="text" size="5" maxlength="5" /></td><td>Strong sides usage level </td>';
		//$block .= '<td><input name="mark2" type="text" size="5" maxlength="5" /></td><td>New possibilities</td></tr>';
		//$block .= '<tr><td><input name="mark3" type="text" size="5" maxlength="5" /></td><td>Weak sides removal level</td>';
		//$block .= '<td><input name="mark4" type="text" size="5" maxlength="5" /></td><td>Level of risk removal</td></tr></table>';
		
		$block .= '</table></td></tr></table>';
		print ($block);
	}
	
	function printNews($arr)
	{
	$i=0;
	$count=count($arr,0);
	 	while($i<$count)
		{
			$id = $arr[$i][0];
			$comments = $arr[$i][2];
			$link="<li><a href = \"interrogation.php?i_id=$id\"> $comments<BR><a>";
			print ($link);
			$i++;
		}
	}
	
}
?>