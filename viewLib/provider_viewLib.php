<?php

class ProviderView {

    function displayList($ilist, $page_count) {
        $i = 0;
        $count = count($ilist, 0);
        while ($i < $count - 1) {
            $i_id = $ilist[$i][0];
            $i_name = stripcslashes($ilist[$i][1]);
            $date = $ilist[$i][2];
            $id_expert = $ilist[$i][3];
            $login = $ilist[$i][4];
            $link = "<li class=\"alt\">

		<div class=\"archive_title\">
		<a href=\"provider_view.php?i_id=$i_id&type=provider\"> $i_name</a>
		</div>
		<div class=\"date\">
		Created $date<br>
		Author: <a href=\"expert.php?id_expert=$id_expert\"> $login </a>
		</div>
		</li>";
            //$link="<li><a href = \"interrogation.php?i_id=$i_id\"> $i_name<BR></a>";
            print ($link);
            $i++;
        }
        print("<br> Page ");
        $i = 1;
        while ($i < $page_count + 1) {
            if ($i == $ilist['page']) {
                print(" -$i- ");
            } else {
                print("<a href=\"provider_list.php?page=$i\"> -$i- </a>");
            }
            $i = $i + 1;
        }
    }
	
	function printSelect(){
		//$query = "SELECT  subjects, create_date, id_strategy FROM strategy order by subjects";
		$query = "SELECT IDPR, NAME, TYPE, EXDATA FROM providers ORDER BY NAME";
        $result = mysql_query($query);
		$html_str = '<select id="provider">';
        while ($name_row = mysql_fetch_row($result)) {
			$html_str .= '<option value="'. $name_row[0].'">'. $name_row[1].' ( '.$name_row[2].' ) </option>';
        }
		$html_str .= '</select>';
		return $html_str;
	}

}

?>