<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                 <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Expert view</h1>
					</div>
				
					<div class="post_body">
					
						<div style = "width:800px; float:left">
						<?php $op = new Opinion();
                        $arrTemp = $op->getStrategyParamMark($_GET['i_id']);
						$arrMark = $op->getStrategyMark($_GET['i_id'],$_GET['o_id'] );
						//print_r( $arrTemp);
                        $w1=$w2=$w3=$w4=$r1=$r2=$r3=$r4=$p1=$p2=$p3=$p4=$s1=$s2=$s3=$s4='&nbsp';
                        $mw1=$mw2=$mw3=$mw4=$mr1=$mr2=$mr3=$mr4=$mp1=$mp2=$mp3=$mp4=$ms1=$ms2=$ms3=$ms4='&nbsp';
						$sids1 = $sidw1 = $sidp1 = $sidr1 = '';
						$cter1 = $cter2 = $cter3 = $cter4 = 1;
						//print_r($arrMark);	
						foreach ($arrTemp as $val => $value) { 
							$subject = $value;
							if ($val == 'Strong')
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){
										$sids1 = '<input type="hidden" value="'.$t_err[0].'" name="hms1" id="hms1" >';
										${'s'.($i+1)} = $t_err[1];
										foreach($arrMark as $val2 => $value2){
											if($t_err[0] == $value2[2] ){
												if(($i+1) == $value2[0]){
													${'ms'.($i+1)} = $value2[1];
												}
											}
										}
									}
								
								}
								
							}
							if ($val == "Weak")
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){	
										$sidw1 = '<input type="hidden" value="'.$t_err[0].'" name="hmw1" id="hmw1" >';
										${'w'.($i+1)} = $t_err[1];
										foreach($arrMark as $val2 => $value2){
											if($t_err[0] == $value2[2] ){
												if(($i+1) == $value2[0]){
													${'mw'.($i+1)} = $value2[1];
												}
											}
										}
						    			
									}
								
								}
								
							}
							if ($val == "New")
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){
										$sidp1 = '<input type="hidden" value="'.$t_err[0].'" name="hmp1" id="hmp1" >';
										${'p'.($i+1)} = $t_err[1];
										foreach($arrMark as $val2 => $value2){
											if($t_err[0] == $value2[2] ){
												if(($i+1) == $value2[0]){
													${'mp'.($i+1)} = $value2[1];
												}
											}
										}
						    			
									}
								
								}
							}
							if ($val == "Risky")
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){
										$sidr1 = '<input type="hidden" value="'.$t_err[0].'" name="hmr1" id="hmr1" >';
										${'r'.($i+1)} = $t_err[1];
										foreach($arrMark as $val2 => $value2){
											if($t_err[0] == $value2[2] ){
												if(($i+1) == $value2[0]){
													${'mr'.($i+1)} = $value2[1];
												}
											}
										}
									}
								
								}
							}
							
						}
						
 						?>
                          <table id="tbl_pull" width="600" style="padding:5px; border:1px solid #ccc;">
                              <tr>
                                <td width="170" rowspan="4">Weak sides</td>
                                <td width="338"><?php echo $w1; ?></td>
                                <td width="36"><?php echo $mw1; echo $sidw1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $w2; ?></td>
                                <td><?php echo $mw2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $w3; ?></td>
                                <td><?php echo $mw3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $w4; ?></td>
                                <td><?php echo $mw4; ?></td>
                              </tr>
                              <tr>
                                <td rowspan="4">Risky sides</td>
                                <td><?php echo $r1; ?></td>
                                <td><?php echo $mr1; echo $sidr1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $r2; ?></td>
                                <td><?php echo $mr2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $r3; ?></td>
                                <td><?php echo $mr3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $r4; ?></td>
                                <td><?php echo $mr4; ?></td>
                              </tr>
                              <tr>
                                <td rowspan="4">New possibilities sides</td>
                                <td><?php echo $p1; ?></td>
                                <td><?php echo $mp1; echo $sidp1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $p2; ?></td>
                                <td><?php echo $mp2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $p3; ?></td>
                                <td><?php echo $mp3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $p4; ?></td>
                                <td><?php echo $mp4; ?></td>
                              </tr>
                              <tr>
                                <td rowspan="4">Strong sides</td>
                                <td><?php echo $s1; ?></td>
                                <td><?php echo $ms1; echo $sids1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $s2; ?></td>
                                <td><?php echo $ms2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $s3; ?></td>
                                <td><?php echo $ms3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $s4; ?></td>
                                <td><?php echo $ms4; ?></td>
                              </tr>
                            </table>
                          </div>
					<?php 
					if(isSet($_GET['o_id']))
					{
		    		$i=new Alternative();
					$id=$_GET['o_id'];
					$arr=$i->getEOpinion($id);
					$j=new AlternativeView();
					$j->printEOpinion($arr, $id);
					if(isSet($_SESSION['id_expert']))
					{
						$id_expert=$_SESSION['id_expert'];
						
						$l=new EstimationOpinion();
						$id_opinion=$_GET['o_id'];
						$i_id=$_GET['i_id'];
						$is_admin=$_SESSION['is_admin'];
						if(($l->isOtherExpert($id_opinion,$id_expert))||($is_admin==1))
						{
							print("<form  method=\"post\" action=\"delete_estimation_action.php?opinion_id=$id_opinion&i_id=$i_id\">");
							print("<label><input type=\"submit\"  value=\"Delete\" /></label>");
							print("</form>");
							
						}
						if($l->isOtherExpert($id_opinion,$id_expert))
						{
							print("<form  method=\"post\" action=\"update_estimation.php?opinion_id=$id_opinion&i_id=$i_id\">");
							print("<label><input type=\"submit\"  value=\"Change\" /></label>");
							print("</form><br>");
						}
					}
					else
					{
						//print("<BR>not set");
					}
					}
					else
					{
						print("<h2>Error!</h2>");
					}
    				?>
					</div>
					<div class="post" >
					<div class="post_title">
						<h1>Marks of the expert view</h1>
					</div>
				
					<div class="post_body">
					
					
					
					<?php 
					if(isSet($_GET['o_id']))
						{
				    		$i=new Alternative();
							$id=$_GET['o_id'];
							$arr=$i->getEstimationOpinion($id);
							$j=new AlternativeView();
							$id_expert1;
							if(isSet($_SESSION['id_expert']))
							{
							$id_expert1=$_SESSION['id_expert'];
							}
							else
							{
								$id_expert1=0;
							}
							$is_admin=0;
							if(isSet($_SESSION['is_admin']))
							{
							$is_admin=$_SESSION['is_admin'];
							}
							$id_opinion=$_GET['o_id'];
							$i_id=$_GET['i_id'];
							$j->printEstimationOpinion($arr,$id_expert1,$id_opinion,$i_id,$is_admin);
						}
				    ?>
					</div>
					</div>
					
					<div class="post" >
					<div class="post_title">
						<h1>Addition of a mark</h1>
					</div>
				
					
                    <div class="post_body">
					<br>
					<?php 
					if(isSet($_GET['o_id'])&&isSet($_GET['i_id']))
						{
					$d=$_GET['i_id'];
					$s=$_GET['o_id'];
					print("<a href = \"add_estimation.php?i_id=$d&o_id=$s\">Add a mark to the view</a><BR>");
					//print("<a href = \"interrogation.php?i_id=$d\">Back</a><BR>");
						}
					?>
					<br>
					</div>
					</div>
				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>