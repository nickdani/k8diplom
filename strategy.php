<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li class="current_page_item"><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Strategies list</h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
						<?php
						$page=1;
						$onpage=10;
							$i=new Strategy();
							$interrList = $i->getList($page,$onpage);
							$page_count=$i->getNumOfPages($onpage);
							$j=new StrategyView();
							$j->displayList($interrList,$page_count);
					   ?>
					  
					 </ol>
					</div>

				</div>
		
			</div>

		</div>

		
<?php 
include_once("end.php");
?>