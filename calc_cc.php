<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
                	<div class="post_title" style="padding-bottom:10px;border-bottom:1px solid #fff;"><h1>Calculate Coefficient of concordance</h1></div>
                	<div class="post_title"><h1>Select provider </h1></div>
                    <div class="post_body" style="padding-bottom:10px;">
                    <?php $str = new StrategyView();
						echo $str->printSelect();
					?>
                   
                    </div>
					
					<div class="post_title"><h1>Results of calculation: </h1></div>
					<div id="result_cc" class="post_body "style="padding-bottom:10px;">
					
					</div>
					<div class="post_body" style="padding-bottom:10px;">
                    	<input type="button" value="Calculate" id="calc_but">
                    </div>
				</div>
		
			</div>

		</div>

		
<?php 
include_once("end.php");
?>