<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                 <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Results</h1>
					</div>
				
					<div class="post_body ">
					<ol>
						<?php
						if(isSet($_GET['interrogation_id'])&&isSet($_SESSION['id_expert']))
						{
							$interrogation_id=$_GET['interrogation_id'];
							$expert_id=$_SESSION['id_expert'];
							$experience=new Experience();
							$experience->deleteInterrogationExperience($interrogation_id);
							$interrogation=new Interrogation();
							$interrogation->deleteInterrogation($interrogation_id);
							
							print("<h2>You have deleted pools</h2>");
							print("<a href=\"update_expert.php?id_expert=$expert_id\">Back</a>");
						}
						else
						{
							print("<h2>Error!</h2>");
						}
					   ?>
					 </ol>
					</div>

				</div>
		
			</div>

		</div>

		<?php 
include_once("end.php");
?>