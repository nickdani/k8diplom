<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
                                <li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
               <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Change the view mark</h1>
					</div>
				
					<div class="post_body">
					
						<?php 
					if(isSet($_GET['opinion_id'])&&isSet($_GET['expert_id'])&&isSet($_GET['i_id']))
					{
					$id_opinion=$_GET['opinion_id'];
					$id_expert=$_GET['expert_id'];
					$i_id=$_GET['i_id'];
					//$opinion=new Opinion();
					
					
					print("<form  method=\"post\" action=\"update_opinion_estimation_action.php?id_opinion
					=$id_opinion&id_expert=$id_expert&i_id=$i_id\">");
					
					  print("<select name=\"jumpMenu_update_opinion_estimation\" onchange=\"MM_jumpMenu('parent',this,0)\">");
					 
					  $alternative=new Alternative();
					  $mark=$alternative->getMarkEstimationOpinion($id_opinion,$id_expert);
					  switch($mark)
								{
								
								case -2:
									print(" 
										    <option  selected=\"selected\" value=\"-2\">don't agree</option>
										    <option value=\"-1\">probably don't agree</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    
										   
										   ");
									break;
								case -1:
									print(" 
										    <option value=\"-2\">don't agree</option>
										    <option  selected=\"selected\" value=\"-1\">probably don't agree</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										   ");
									break;
								
									case 0:
									print(" 
										    <option value=\"-2\">don't agree</option>
										    <option value=\"-1\">probably don't agree</option>
										    <option  selected=\"selected\" value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										   ");
									break;
									
								case 1:
									print("
										    <option value=\"-2\">don't agree</option>
										    <option value=\"-1\">probably don't agree</option>
										    <option value=\"0\">neutral</option>
										    <option  selected=\"selected\" value=\"+1\">probably agree</option>
										    <option value=\"+2\">agree</option>
										    ");
									break;
								case 2:
									print(" 
										    <option value=\"-2\">don't agree</option>
										    <option value=\"-1\">probably don't agree</option>
										    <option value=\"0\">neutral</option>
										    <option value=\"+1\">probably agree</option>
										    <option  selected=\"selected\" value=\"+2\">agree</option>
										   ");
									break;
								
								}
					 
					  print("</select>
					
					   <BR> Comment:  <BR>
					  <label>
					  <textarea name=\"comments_update_opinion_estimation\"  cols=\"45\" rows=\"5\">
					");
					//$comments=$opinion->getComments($s);
					$comments=$alternative->getCommentsEstimationOpinion($id_opinion,$id_expert);
					print($comments);
					//print(trim($comments));
					print("</textarea>
					  </label>
					
					  <br>
					  <label>
					  <input type=\"submit\"  value=\"Change\" />
					  </label>");
					  
					  print("</form>");
					  print("<BR><a href = \"estimation.php?i_id=$i_id&o_id=$id_opinion\"> Back<BR></a>");
					
					}
					else
					{
						print("<h2>Error!</h2>");
					}
					  ?>
					
					</div>

				</div>
		
			</div>

		</div>
		

<?php 
include_once("end.php");
?>