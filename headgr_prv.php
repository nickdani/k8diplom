<?php
	//error_reporting(0);
	include_once ("conf.php");



//$filename = "data/first.txt";
//$handle = fopen($filename, "r");
//$strdata = fread($handle, filesize($filename));
//fclose($handle);
			?>
<!doctype html>
<html><head>
<meta charset="windows-1251">
<title>Diplomwork</title>
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<link href="css/common.css" rel="stylesheet" type="text/css">
<link href="css/custom-theme/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css">

<script src="js/jquery-1.8.3.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>

<script type="text/javascript">

	$(function() {
		// hide dialog window
		$("#save_graph_wnd").hide(); 
		$("#alert_d").hide();
		$("#cont_error").hide();

		//Tool bar
		$("#save_provider_btn").button({icons:{primary:"ui-icon-disk"},text:true});
		
		$("#save_provider_btn").click(function(e) {
			e.preventDefault();
			//alert("Save strtatagy");
			// Checking input fields
			 var reg_exp1 = /^[0-9a-zA-Z�-��-߳�����\-\(\)\. ]{3,}$/i;
			 var reg_exp2 = /^[0-9]{1,}\.{0,1}[0-9]{1,}$/i;
			 var text_msg1 = ' only latin letter and number are allowed!'
			  var text_msg2 = ' only float number are allowed!'
			 
			if(checkLength2($("#name_provider"), 'Name provider', 3, 50) === false){return;}
			if(checkRegexp2($("#name_provider"), reg_exp1, 'Name provider', text_msg1 ) === false){return;}
			
			if(checkLength2($("#alt_form #comment"), 'Comment', 3, 50) === false){return;}
			if(checkRegexp2($("#alt_form #comment"), reg_exp1, 'Comment', text_msg1 ) === false){return;}
			
			if(checkLength2($("#alt_form #que1"), 'Factor 1', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que1"), reg_exp1, ' Factor 1', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k1"), 'k1', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k1"), reg_exp2, ' K1', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que2"), 'Factor 2', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que2"), reg_exp1, ' Factor 2', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k2"), 'k2', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k2"), reg_exp2, ' K2', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que3"), 'Factor 3', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que3"), reg_exp1, ' Factor 3', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k3"), 'k3', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k3"), reg_exp2, ' K3', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que4"), 'Factor 4', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que4"), reg_exp1, ' Factor 4', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k4"), 'k4', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k4"), reg_exp2, ' K4', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que5"), 'Factor 5', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que5"), reg_exp1, ' Factor 5', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k5"), 'k5', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k5"), reg_exp2, ' K5', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que6"), 'Factor 6', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que6"), reg_exp1, ' Factor 6', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k6"), 'k6', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k6"), reg_exp2, ' K6', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que7"), 'Factor 7', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que7"), reg_exp1, 'Factor 7', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k7"), 'k7', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k7"), reg_exp2, ' K7', text_msg2 ) === false){return;}
			
			if(checkLength2($("#alt_form #que8"), 'Factor 8', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #que8"), reg_exp1, ' Factor 8', text_msg1 ) === false){return;}
			if(checkLength2($("#alt_form #k8"), 'k8', 1, 50) === false){return;}
			if(checkRegexp2($("#alt_form #k8"), reg_exp2, ' K8', text_msg2 ) === false){return;}
			
			var post = {};
			post.name_provider = $("#name_provider").val();
			post.comment = $("#alt_form #comment").val();
			// k provider
			post.k1 = $("#k1").val();
			post.k2 = $("#k2").val();
			post.k3 = $("#k3").val();
			post.k4 = $("#k4").val();
			post.k5 = $("#k5").val();
			post.k6 = $("#k6").val();
			post.k7 = $("#k7").val();
			post.k8 = $("#k8").val();
			// query
			post.que1 = $("#que1").val();
			post.que2 = $("#que2").val();
			post.que3 = $("#que3").val();
			post.que4 = $("#que4").val();
			post.que5 = $("#que5").val();
			post.que6 = $("#que6").val();
			post.que7 = $("#que7").val();
			post.que8 = $("#que8").val();
			//post.filename = image_name;
			
			var url = 'save_provider.php';
			$.post(url, post, function(data, status) {
				//alert(status);
				if(status == 'success'){
					//image_name = 'none'
					showMessage('Message', '<p>'+data+'</p>', 300, "Close");
				} else {
					// Server error
					showMessage('Message', '<p>File does not save! Server error.</p>'+data, 400, "Close");
				}
			});
		});
		
		var file_name = $("#file_name_ti");
		// ����� ��������� ��� �������� ���������
		var vpFields = $([]).add(file_name);
		// Dialog window
		var tips_vp = $("#tips_vp");
		
    });
	
	// Helper functions

	function checkLength2(o, n, min, max) {
		var error = $("#cont_error");
		if (o.val().length > max || o.val().length < min) {
			error.get(0).innerHTML = "Field <i><b>"+n+"</b></i> ranges from " + min + " to " + max + " symbols.";
			error.show();
			o.focus();
			return false;
		} else {
			error.hide();
			return true;
		}
	}
	function checkRegexp2(o, regexp, n, text_error) {
		//console.info(o);
		
		var error = $("#cont_error");
		if (!(regexp.test(o.val()))) {
			error.get(0).innerHTML = "In field <i><b>"+n+"</b></i> "+text_error;
			error.show();
			o.focus();
			return false;
		} else {
			error.hide();
			return true;
		}
	}
	
	function checkLength(o, n, min, max, tc) {
		if (o.val().length > max || o.val().length < min) {
			o.addClass("ui-state-error");
			o.focus();
			updateTips("Field <i>"+n+"</i> ranges from " +
				min + " to " + max + " symbols.",tc);
			return false;
		} else {
			return true;
		}
	}
	
	function checkRegexp(o, regexp, n,tc) {
		if (!(regexp.test(o.val()))) {
			o.addClass("ui-state-error");
			updateTips(n,tc);
			o.focus();
			return false;
		} else {
			return true;
		}
	}
	function updateTips(t,tc) {
		tc.html(t)
			.addClass("ui-state-highlight");
		setTimeout(function() {
			tc.removeClass("ui-state-highlight", 1500);
		}, 500);
	}
	
	function showMessage(title, msg, min_w, text_but) {
	
		$("#alert_d").get(0).title = title;
		$("#alert_d").get(0).innerHTML = msg;
		$( "#alert_d" ).dialog({modal: true, minWidth: min_w, buttons:[{text: text_but,
		click: function(){$(this).dialog("close");$(this).dialog("destroy");}}]});
	}
	
</script>


</head>

<body>
<div id="layout_wrapper">
<div id="layout_edgetop"></div>

<div id="layout_container">

	<div id="site_title">

		<h1 class="left">Automated system for the alternatives selection </h1>
		<h2 class="right"></h2>

		<div class="clearer">&nbsp;</div>

	</div>

<div id="top_separator"></div>

