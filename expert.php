<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providerds</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New vies</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                 <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Expert</h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
					<div class="comment_gravatar left">
									<img alt="" src="img/sample-gravatar.jpg" height="32" width="32" />
								</div>
						<?php
						if(isSet($_GET['id_expert']))
						{
							$id_exp=$_GET['id_expert'];
							$expert = new Expert();
							$arr=$expert->getDescriptionExpert($id_exp);
							$expert_view=new ExpertView();
							$expert_view->printExpertDescription($arr);
						}
						else 
						{
							print("<h2>Error!</h2>");
						}
					   ?>
					 </ol>
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>