<?php 
//error_reporting(0);
include_once("head.php");
if($_SESSION['is_admin']){$isadmin = true;} else {$isadmin = false;}

?>

<div id="navigation">
    <div id="tabs">
        <ul>
            <li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            	<li><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
        </ul>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
<div class="spacer h5"></div>
<div id="main">
	<div class="left" id="main_left">
		<div id="main_left_content">
        	 <div class="post_title"><h1 style="font-size:16px; font-weight:300;">
				Pairwise comparison of factors</h1>
             </div>
        	<div style="border:1px solid #c6d2db; padding:1px; margin-top:5px; margin-bottom:5px;" >
            <form id="alt_form" name="alt_form" action="" enctype="application/x-www-form-urlencoded">
            <div id="cont_error" style="padding:5px; background-color:#FFC; margin:5px; border:1px solid red;">Error</div>
            <div id="cont_factor"></div>
            <div style="border-top:1px solid #c6d2db;">
            <div style="width:430px; height:50px; line-height:50px; margin-left:auto; margin-right:auto;">
            	<button id="caculate_cf_btn" >Calculate</button>
            </div>
            </div>
            </form>
            </div>

		</div>
	</div>
</div>       
 
<!--  Dialog window -->
<div id="alert_d" title="Basic dialog"><p>
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    Your files have downloaded successfully into the My Downloads folder.
	</p>
</div>





<?php 
include_once("end.php");
?>