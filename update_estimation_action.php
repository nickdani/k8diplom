<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Result</h1>
					</div>
				
					<div class="post_body">
					<?php 
					if(isSet($_GET['int_id'])&&isSet($_GET['op_id'])
					&&isSet($_POST['jumpMenu_update_estimation'])&&isSet($_POST['comments_update_estimation']))
					{
					include_once ("conf.php");
					$interrogation_id=$_GET['int_id'];
					$opinion_id=$_GET['op_id'];
					$preference=$_POST['jumpMenu_update_estimation'];
					$comments=addslashes($_POST['comments_update_estimation']);
					print("<h2>Changes are saved</h2>");
					$opinion=new Opinion();
					$opinion->updateOpinion($opinion_id,$preference,$comments);
					print("<BR><a href = \"estimation.php?i_id=$interrogation_id&o_id=$opinion_id\"> Back<BR></a>");
					}
					else
					{
						print("<h2>Error!</h2>");
					}
					?>
					
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>