<?php
include_once("head.php");
?>
<script type="text/javascript">
   	$(function() {
	
		$("#but1").click(function(e) {
			//alert('Send form');
			/*
			var comment_result = ' --- '+$("#comments").val()+' --- ';
			comment_result += 'Strong sides usage level = '+$("#mark1_1").val()+' : '+$("#mark1_2").val()+' : '+$("#mark1_3").val()+' : '+$("#mark1_4").val()+'. ';
			comment_result += 'New possibilities = '+$("#mark2_1").val()+' : '+$("#mark2_2").val()+' : '+$("#mark2_3").val()+' : '+$("#mark2_4").val()+'. ';
			comment_result += 'Weak sides removal level = '+$("#mark3_1").val()+' : '+$("#mark3_2").val()+' : '+$("#mark3_3").val()+'. ';
			comment_result += 'Level of risk removal = '+$("#mark4_1").val()+' : '+$("#mark4_2").val()+'. ';
			$("#comments").val(comment_result);
			*/
			$("#form1").get(0).submit();
			
		});
	
	});
</script>

<div id="navigation">

    <div id="tabs">

        <ul>
            <!-- <li><a href="index.php"><span>Провайдеры</span></a></li>-->
            <li><a href="strategy.php"><span>Strategies</span></a></li>
            <li ><a href="news.php"><span>New views</span></a></li>
            <li><a href="expert_list.php"><span>Experts</span></a></li>
             <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>

        </ul>

        <div class="clearer">&nbsp;</div>

    </div>

</div>

<div class="spacer h5"></div>

<div id="main">

    <div class="left" id="main_left">

        <div id="main_left_content">

            <div class="post">

                <div class="post_title">
                    <h1>Addition of a view</h1>
                </div>

                <div class="post_body">

                    <?php
                    if (isSet($_SESSION['id_expert'])) {
                        if (isSet($_GET['i_id'])) {
                            $d = $_GET['i_id'];
                            if ($_GET['type'] == 'strategy') { ?>
                                <form  id="form1" method="post" action="add_opinion_action.php?int_id=<?php echo $d; ?>&type=strategy" >
                            <?php } else { ?>
                                <form id="form1"  method="post" action="add_opinion_action.php?int_id=<?php echo $d; ?>" >
                           <?php } ?>
                          &nbsp;&nbsp;The level of alternative preferrence:&nbsp;
						  
                          <select name="jumpMenu" onchange="MM_jumpMenu('parent',this,0)" >
						    <option value="-3">totally against</option>
						    <option value="-2">against</option>
						    <option value="-1">probably against</option>
						    <option selected="selected" value="0">neutral</option>
						    <option value="+1">probably agree</option>
						    <option value="+2">agree</option>
						    <option value="+3">totally agree</option>
						  </select></br></br>
                          
						<?php $op = new Opinion();
                        $arrTemp = $op->getStrategyParamMark($d);
                        $w1=$w2=$w3=$w4=$r1=$r2=$r3=$r4=$p1=$p2=$p3=$p4=$s1=$s2=$s3=$s4='&nbsp';
                        $mw1=$mw2=$mw3=$mw4=$mr1=$mr2=$mr3=$mr4=$mp1=$mp2=$mp3=$mp4=$ms1=$ms2=$ms3=$ms4='&nbsp';
						$sids1 = $sidw1 = $sidp1 = $sidr1 = '';
                        $str_opt  = '<option value="1">1</option>';
						$str_opt .= '<option value="2">2</option>';
						$str_opt .= '<option value="3">3</option>';
						$str_opt .= '<option value="4">4</option>';
						$str_opt .= '<option value="5">5</option>';
						$str_opt .= '<option value="6">6</option>';
						$str_opt .= '<option value="7">7</option>';
						$str_opt .= '<option value="8">8</option>';
						$str_opt .= '<option value="9">9</option>';
						$str_opt .= '<option value="10">10</option></select>';    

						$cter1 = $cter2 = $cter3 = $cter4 = 1;
						//print_r($arrTemp);	
						foreach ($arrTemp as $val => $value) { 
							$subject = $value;
							if ($val == 'Strong')
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){
										$sids1 = '<input type="hidden" value="'.$t_err[0].'" name="hms1" id="hms1" >';
										${'s'.($i+1)} = $t_err[1];
										${'s'.($i+1)} .= '<input type="hidden" value="'.(1+$i).'" name="hts'.($i+1).'" id="hts'.($i+1).'" >';
										
										${'ms'.($i+1)} = '<select name="ms'.($i+1).'" id="ms'.($i+1).'" >';
						    			${'ms'.($i+1)} .= $str_opt;
									}
								
								}
								
							}
							if ($val == "Weak")
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){	
										$sidw1 = '<input type="hidden" value="'.$t_err[0].'" name="hmw1" id="hmw1" >';
										${'w'.($i+1)} = $t_err[1];
										${'w'.($i+1)} .= '<input type="hidden" value="'.(1+$i).'" name="htw'.($i+1).'" id="htw'.($i+1).'" >';
										${'mw'.($i+1)} = '<select name="mw'.($i+1).'" id="mw'.($i+1).'" >';
						    			${'mw'.($i+1)} .= $str_opt;
									}
								
								}
								
							}
							if ($val == "New")
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){
										$sidp1 = '<input type="hidden" value="'.$t_err[0].'" name="hmp1" id="hmp1" >';
										${'p'.($i+1)} = $t_err[1];
										${'p'.($i+1)}  .= '<input type="hidden" value="'.(1+$i).'" name="htp'.($i+1).'" id="htp'.($i+1).'" >';
										${'mp'.($i+1)} = '<select name="mp'.($i+1).'" id="mp'.($i+1).'" >';
						    			${'mp'.($i+1)} .= $str_opt;
									}
								
								}
							}
							if ($val == "Risky")
							{
								for ($i = 0; $i<4; $i++) {
									$t_err = explode(':',$subject[$i]);
									if(strlen($t_err[1]) > 0){
										$sidr1 = '<input type="hidden" value="'.$t_err[0].'" name="hmr1" id="hmr1" >';
										${'r'.($i+1)} = $t_err[1];
										${'r'.($i+1)}  .= '<input type="hidden" value="'.(1+$i).'" name="htr'.($i+1).'" id="htr'.($i+1).'" >';
										${'mr'.($i+1)} = '<select name="mr'.($i+1).'" id="mr'.($i+1).'" >';
						    			${'mr'.($i+1)} .= $str_opt;
									}
								
								}
							}
							
						}
						
						?>
                            
                            <table id="tbl_pull" width="600" style="padding:5px; border:1px solid #ccc;">
                              <tr>
                                <td width="170" rowspan="4">Weak sides</td>
                                <td width="338"><?php echo $w1; ?></td>
                                <td width="36"><?php echo $mw1; echo $sidw1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $w2; ?></td>
                                <td><?php echo $mw2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $w3; ?></td>
                                <td><?php echo $mw3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $w4; ?></td>
                                <td><?php echo $mw4; ?></td>
                              </tr>
                              <tr>
                                <td rowspan="4">Risky sides</td>
                                <td><?php echo $r1; ?></td>
                                <td><?php echo $mr1; echo $sidr1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $r2; ?></td>
                                <td><?php echo $mr2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $r3; ?></td>
                                <td><?php echo $mr3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $r4; ?></td>
                                <td><?php echo $mr4; ?></td>
                              </tr>
                              <tr>
                                <td rowspan="4">New possibilities sides</td>
                                <td><?php echo $p1; ?></td>
                                <td><?php echo $mp1; echo $sidp1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $p2; ?></td>
                                <td><?php echo $mp2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $p3; ?></td>
                                <td><?php echo $mp3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $p4; ?></td>
                                <td><?php echo $mp4; ?></td>
                              </tr>
                              <tr>
                                <td rowspan="4">Strong sides</td>
                                <td><?php echo $s1; ?></td>
                                <td><?php echo $ms1; echo $sids1; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $s2; ?></td>
                                <td><?php echo $ms2; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $s3; ?></td>
                                <td><?php echo $ms3; ?></td>
                              </tr>
                              <tr>
                                <td><?php echo $s4; ?></td>
                                <td><?php echo $ms4; ?></td>
                              </tr>
                            </table>
                                                       <BR>Comment:  <BR>
						  <label>
						  <textarea name="comments" id="comments"   cols="45" rows="5"></textarea>
						  </label>
						
						  <br><br>
						  <label><input type="button" id="but1"  value="Add" /></label>
                          <?php
                            $d = $_GET['i_id'];
						  ?>
                            <p><a href = "interrogation.php?i_id=<?php echo $d; ?>"> Back</a></p>
                            </form>
                        <?php } else { ?>
                            <h2>Error!</h2>
                        <?php } 
                    } else {
                        $d = $_GET['i_id'];
						?>
                        <h2>You haven't logged in!</h2>
                        <p><a href = "interrogation.php?i_id=<?php echo $d; ?>"> Back</a></p>
                   <?php } ?>

                </div>

            </div>

        </div>

    </div>

    <?php
                    include_once("end.php");
    ?>