<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Registrations</h1>
					</div>
				
					<div class="post_body">
					   <?php 
						include_once ("conf.php");
						//print("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=cp1251\" />");
						//session_unregister('id_expert');
						unset( $_SESSION['id_expert'] );
						//session_unregister('is_admin');
						unset( $_SESSION['is_admin'] );
						print("<h2>You have logged out </h2>");
						?>
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>