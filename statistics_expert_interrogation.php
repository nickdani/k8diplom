<?php 
include_once("head.php");
?>

	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<li><a href="provider.php"><span>Providers</span></a></li>
                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
               <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Statistics of trust level
						</h1>
					</div>
				
					<div class="post_body nicelist">
					<ol>
						<?php
						if(isSet($_GET['interrogation_id']))
						{
							$id_interrogation=$_GET['interrogation_id'];
							$statistics=new Statistics();
							$arr=$statistics->getExpertStatisticsOnTrustLevel($id_interrogation);
							$st=new StatisticsView();
							$st->printExpertStatisticsOnTrustLevel($arr,10);
						}
						else
						{
							print("<h2>Error!</h2>");
						}
					   ?>
					 </ol>
					</div>

				</div>
		
			</div>

		</div>

		
<?php 
include_once("end.php");
?>
