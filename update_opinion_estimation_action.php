<?php 
include_once("head.php");
?>


	<div id="navigation">

		<div id="tabs">
			
			<ul>
				<!-- <li><a href="index.php"><span>Providers</span></a></li> -->
                                <li><a href="strategy.php"><span>Strategies</span></a></li>
				<li ><a href="news.php"><span>New views</span></a></li>
				<li><a href="expert_list.php"><span>Experts</span></a></li>
                <?php if($_SESSION['is_admin']) {?>
            <li "><a href="main_graph.php"><span>Graph building</span></a></li>
            <?php } ?>
				
			</ul>

			<div class="clearer">&nbsp;</div>

		</div>

	</div>

	<div class="spacer h5"></div>

	<div id="main">

		<div class="left" id="main_left">

			<div id="main_left_content">		

				<div class="post">
					
					<div class="post_title">
						<h1>Results</h1>
					</div>
				
					<div class="post_body">
					<?php 
					if(isSet($_GET['id_expert'])&&isSet($_GET['id_opinion'])
					&&isSet($_GET['i_id']))
					{
					include_once ("conf.php");
					$id_expert=$_GET['id_expert'];
					$opinion_id=$_GET['id_opinion'];
					$i_id=$_GET['i_id'];
					$mark=$_POST['jumpMenu_update_opinion_estimation'];
					$comments=addslashes($_POST['comments_update_opinion_estimation']);
					print("<h2>Changes are saved</h2>");
					//$opinion=new Opinion();
					//$opinion->updateOpinion($opinion_id,$preference,$comments);
					$alernative=new Alternative();
					$alernative->updateEstimationOpinion($opinion_id,$id_expert,$comments,$mark);
					$est_op=new EstimationOpinion();
						$est_op->setConsentLevel($opinion_id);
					print("<BR><a href = \"estimation.php?i_id=$i_id&o_id=$opinion_id\">Back<BR></a>");
					}
					else
					{
						print("<h2>Error!</h2>");
					}
					?>
					
					</div>

				</div>
		
			</div>

		</div>

<?php 
include_once("end.php");
?>